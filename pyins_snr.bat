set icon=DNR.ico
set srcdir=.
set bindir=D:\UNW\gui-lite\bin
set tmpdir=D:\UNW\gui-lite\tmp

rmdir /s /q %bindir%\mainexec
rmdir /s /q %bindir%\main


pyi-makespec ^
  --add-data="%srcdir%\config.ini;." ^
  --add-data="%srcdir%\%icon%;." ^
  --add-data="%srcdir%\logo.png;." ^
  --hidden-import fiona._shim ^
  --hidden-import fiona.schema ^
  --additional-hooks-dir="%srcdir%\pyhook" ^
  --exclude-module=PyQt4 ^
  --exclude-module=PyQt5 ^
  --exclude-module=Tkinter ^
  --exclude-module=gtk ^
  --nowindowed ^
  -i "%srcdir%\%icon%" ^
  main.py > compile.log 2>&1
  
echo import sys > mainexec.spec
echo sys.setrecursionlimit(5000)  >> mainexec.spec
type main.spec >> mainexec.spec


pyinstaller  --noconfirm --log-level=WARN ^
	--distpath=%bindir% ^
	--workpath=%tmpdir% ^
  --icon="%srcdir%\%icon%" ^
  mainexec.spec >> compile.log 2>&1
	

