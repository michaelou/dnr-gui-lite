# -*- coding: utf-8 -*-
"""
Created on Mon Aug 20 07:38:55 2018

@author: s-gou1
"""
import os
from flopy.utils.zonbud import ZoneBudget

import pandas as pd
import numpy as np
import wx
from io import StringIO
from const import ACRE2FOOT
import matplotlib.pyplot as plt


flow_name = dict(WELLS='Groundwater Pumping', STREAM_LEAKAGE='Baseflow (Stream)', STORAGE='Storage Change')
        
class Modflow():
    def __init__(self, mfNam, namefile, historical=True):
        '''
        read the MODFLOW name file
        '''
        os.chdir(namefile['MASTER'])
        hds_no = int(namefile['NHDS'].split()[-1])
        ddn_no = int(namefile['NDDN'].split()[-1])
        cbb_no = int(namefile['NCBB'].split()[-1])
        self.name = 'modflow'
        nam = pd.read_csv(mfNam, skipinitialspace=True, sep=' ', names=['type', 'fileno', 'file'], comment='#', skip_blank_lines=True)
        nam.loc[:,'type'] = [str(s).upper() for s in nam.loc[:,'type']]
        nam['output'] = nam.loc[:,'type']
        #print(nam)
        for i, r in nam.iterrows():
            nam.iloc[i, 3] = r['type'].upper().strip()
            if int(r['fileno']) == hds_no: nam.iloc[i, 3] = 'HDS'
            if int(r['fileno']) == ddn_no: nam.iloc[i, 3] = 'DDN'
            if int(r['fileno']) == cbb_no: nam.iloc[i, 3] = 'CBC'
        self.name = nam.set_index('output')
        self.run_complete = False

        # read dimensions
        self.mf_read_dis_dim()

        # read time
        iPeriod = 0 if historical else 1
        self.mf_read_dis_time(int(namefile['MFDISSKIP']), namefile['MFSDATE'].split()[iPeriod])




    def mf_read_csv2(self, csv2file, panel=False, sdate='1950-01-01', timeunit='D', offset=0.0, factor=1.0):
        """
        read the CSV2 file generated dby Zonbud
        """
        csv = pd.read_csv(csv2file)
        names = [c.strip() for c in csv.columns]
        csv.columns = names
        col_in = names.index('ZONE') + 1
        col_out = names.index('Total IN') + 1
        nterm = col_out - col_in - 1
        dfin = csv.iloc[:, col_in:(col_in + nterm)]
        dfout = csv.iloc[:, col_out:(col_out + nterm)]
        dfnet = csv.iloc[:, 0:(col_in + nterm)]
        dfnet.iloc[:, col_in:(col_in + nterm)] = (
            dfin.values - dfout.values - offset) * factor
        # many columns
        dfnet['Time'] = pd.to_datetime(sdate) + pd.to_timedelta(dfnet['TOTIM'], unit=timeunit)
        dfnet.set_index('Time', inplace=True)
        return dfnet

    def mf_read_dis_time(self, nskip=0, start_time='1950-01-01'):
        """
        create a time index based on the DIS file
        """
        dis_file = self.name.set_index('type').loc['DIS', 'file']

        dis_time = pd.read_csv(
            dis_file,
            skiprows=nskip,
            sep=' ',
            skipinitialspace=True,
            nrows=self.nper,
            names='PERLEN NSTP TSMULT SsTr'.split())
        #nstep = dis_time['NSTP']
        time = []
        dtt = []
        period = []
        step = []
        cumtim = 0
        for i, row in dis_time.iterrows():
            if row['TSMULT'] == 1:
                dt = row['PERLEN'] / row['NSTP']
            else:
                dt = row['PERLEN'] * (row['TSMULT'] - 1) / (
                    row['TSMULT']**row['NSTP'] - 1)
            for stp in range(row['NSTP']):
                cumtim = cumtim + dt
                dtt.append(dt)
                time.append(cumtim)
                period.append(i + 1)
                step.append(stp + 1)
                dt = dt * row['TSMULT']
        self.dis_time = pd.DataFrame({'Period': period,
                             'Step': step,
                             'Date': pd.to_datetime(start_time) + pd.to_timedelta(time, 'D') - pd.to_timedelta(1, 'H'),
                             'Dt' : dtt,
                             'Cumtim' : time}).set_index(['Period', 'Step'])


    def mf_cbc_diff(cbb_file1, cbb_file2, zones):
        """
        calculate the budget difference of two cell by cell flow file
        """
        zb1 = ZoneBudget(cbb_file1, zones)
        zb2 = ZoneBudget(cbb_file2, zones)

        zb_net = zb2.get_dataframes(net=True) - zb1.get_dataframes(net=True)

        return zb_net


    def mf_read_dis_dim(self):
        dis_file = self.name.loc['DIS', 'file']
        with open(dis_file) as fr:
            while True:
                l = fr.readline()
                # NLAY NROW NCOL NPER  ITMUNI LENUNI
                if not l.startswith('#'):
                    break
                #print(l, l.startswith('#'))
            self.nlay, self.nrow, self.ncol, self.nper = [int(i) for i in l.split()[:4]]
            # LAYCBD(NLAY)
            self.laycbd = fr.readline().split()[:self.nlay]

    def mf_read_well(self):
        welfile = self.name.loc['WEL', 'file']

        with open(welfile) as fr:
            while True:
                l = fr.readline()
                # NLAY NROW NCOL NPER  ITMUNI LENUNI
                if not l.startswith('#'):
                    break
            if l.upper().startswith('PARAMETER'):
                npar = int(l.strip('PARAMETER').split()[0])
                if npar > 0:
                    dlg = wx.MessageDialog(self, 'Pumping change Does NOT support well files with PARAMETER', 'Error', wx.OK | wx.ICON_ERROR)
                    dlg.ShowModal()
                    dlg.Destroy()
                    return False
                else:
                    l = fr.readline()

            self.MXACTW = l[:10]
            self.IWELCB = l[10:20]
            self.ITMP = []
            self.welData = []
            for iper in range(self.nper):
                l = fr.readline()
                #print(l)
                ITMP=int(l[:10])
                #if line.startswith('001'): break
                #print(line)
                #break
                if ITMP > 0:
                    pmp = []
                    l = fr.readline().strip()
                    if l.lower().startswith('open/close'):
                        with open(l.split()[1]) as ffr:
                            for i in range(ITMP):
                                pmp.append(ffr.readline()[:40].rstrip('\n'))

                    else:
                        pmp = [l[:40].rstrip('\n')]
                        for i in range(self.ITMP[-1]-1):
                            pmp.append(fr.readline()[:40].rstrip('\n'))

                    p = pd.read_fwf(StringIO(('\n'.join(pmp))), widths=[10,10,10,10], header=None, names=['layer','row','col','rate'])
                self.welData.append(p)
                #print(pmp)
        return(True)


        def mf_load_hds(self):
            """
            load the simulated heads
            """
                  # get the head files
            os.chdir(namefile['MASTER'])
            headbas_f = self.mf_bas.name.loc['HDS','file']
            self.headbas   = HeadFile(headbas_f)

            os.chdir(self.Parent.name['MASTER'])
            headscn_f = self.mf_scn.name.loc['HDS','file']
            self.headscn   = HeadFile(headscn_f)

            # load time
            self.wxChiPlotTime.Clear()
            self.wxChiPlotTime.AppendItems([dt.strftime('%Y-%m-%d') for dt in self.dis_time['Date']])
            self.wxChiPlotTime.Select(0)

            self.loaded = True

            ndim = [int(i) for i in self.name['DIM'].split()]
            self.wxSpinLayer.SetMax(ndim[0])
            self.SetTitle('Groundwater level - "baseline" vs "' + self.outdir + '"')
#
#    def mf_csv2(self, csv2, nskip=0, start_time=None, factor=1/ACRE2FOOT):
#    #    csv2 = 'G:\\dnr\\COHYST\\MyTest_Run22a1_12_23_July2013DocVersion\\result-baseline\\!9base.2.csv'
#    #    dis_file = 'G:\\dnr\\COHYST\\MyTest_Run22a1_12_23_July2013DocVersion\\COHYST2010_22a1_12_23.dis'
#    #    nskip = 557
#    #    start_time = '1984-10-01'
#        dis_file = self.name.set_index('type').loc['DIS', 'file']
#        df_csv = self.mf_read_csv2(csv2, factor=factor)
#        dis_time = mf_read_dis_time(dis_file, nskip=nskip, start_time=start_time)
#        df_csv['TOTIM'] = [dis_time.loc[(row['PERIOD'], row['STEP']), 'Dt'] for i, row, in df_csv.iterrows()]
#        df_csv.iloc[:, 4:] = df_csv.iloc[:, 4:].multiply(df_csv['TOTIM'], axis='index')
#        df_csv.index = [dis_time.loc[(row['PERIOD'], row['STEP']), 'Date'] for i, row, in df_csv.iterrows()]
#        names = []
#        for n in df_csv.columns:
#            if n in mf_flows.index:
#                names.append(mf_flows.loc[n, 'Name'])
#                df_csv.loc[:,n] = df_csv.loc[:,n] * mf_flows.loc[n, 'Sign']
#            else:
#                names.append(n)
#        df_csv.columns = names
#        return(df_csv)

    def zonbud(self, zone, cbc='CBC', factor=1/ACRE2FOOT):
        from flopy.utils.zonbud import ZoneBudget
        # read the time
        #print('cbb', cbb)

        cbc_file = self.name.loc[cbc, 'file']
#        print(zone.zarray, type(zone.zarray))
        zb = ZoneBudget(cbc_file, zone.zarray)
        zb = zb.get_dataframes(net=True)
        df_net = zb.reset_index().drop('totim', axis=1).set_index('name')

        #print('df_net', df_net)

        df_net = df_net[[i in flow_name for i in df_net.index]]
        df_net.index = [flow_name[i] for i in df_net.index]

        df_net.columns = [zone.znames[int(c.strip('ZONE_'))-1] for c in df_net.columns]

        #print('df_net', df_net)
        df_net.loc['Groundwater Pumping'] = - df_net.loc['Groundwater Pumping']
        df_net.loc['Baseflow (Stream)']   = - df_net.loc['Baseflow (Stream)']
        df_net.loc['Storage Change']      = - df_net.loc['Storage Change']

        df_net = df_net * factor
#        df_net['Time'] = np.tile(self.dis_time['Date'].values,
#              int(df_net.shape[0]/self.dis_time.shape[0]))
        return df_net



    def zonplot(self, df, dfLblcol='Model', ax=None, selectedFlow=[], selectedZone=[],
                AggZone=False, AggVar=False, CumSum=False, plotType='bar', stacked=False, month=False,
                xlbl='', ylbl='', tle=''):
        
        if ax is None:
            ax = plt.gca()
        if len(selectedFlow) > 0:
            df = df.loc[selectedFlow]

        # aggregate varables
        if AggVar:
            d=0
            for f in df.index.unique():
                d += df.loc[f]
            df.index = ''



        df.index.name = 'Flow'
        df = df.reset_index().set_index(['Flow', dfLblcol])
        if len(selectedZone) > 0:
            df = df.loc[:,selectedZone]

        # aggregate zones
        if AggZone:
            df[''] = df.sum(axis=1)
            df = df.iloc[:, -1:]


        #
        ntim = int(df.shape[0] / self.dis_time.shape[0])

        #print('ntim',ntim)
        #print(df.shape)
        #df.index = pd.DatetimeIndex(np.tile(self.dis_time['Date'], ntim))

        # convert rate to volume
        df[:] = (df.T.values * np.tile(self.dis_time['Dt'], ntim)).T
        


        dfPlot = []

        for f in df.index.levels[0]:
            d1 = df.loc[f]
            for m in df.index.levels[1]:
                d = d1.loc[m]
                d.index = self.dis_time['Date']
                if month:
                    e = d.resample('M').sum()
                else:
                    e = d.resample('A').sum()
                        #e.index = e.index.year
                e.columns = [(m + ' ' + z + ' ' + f).strip() for z in e.columns]
                if CumSum:
                    e = e.cumsum()
                dfPlot.append(e)
        dfPlot = pd.concat(dfPlot, axis=1)
        if plotType == 'line':
            dfPlot.plot.line(ax=ax, legend=True, alpha=1.0, stacked=False)
        elif plotType == 'bar':
            dfPlot.index = dfPlot.index.year * 100 + dfPlot.index.month if month else dfPlot.index.year
            dfPlot.plot.bar(ax=ax, legend=True, alpha=0.7, stacked=stacked, linewidth=0)
        else:
            alpha=0.7 if stacked else 0.4
            dfPlot.plot.area(ax=ax, legend=True, alpha=alpha, stacked=stacked, linewidth=0)


        if xlbl != '':
            ax.set_xlabel(xlbl)
        else:
            ax.set_xlabel('Time')
        if ylbl != '':
            ax.set_ylabel(ylbl)
        else:
            ylab = 'Flow (acre-feet per ' + ('year)' if not month else 'month)')
            ax.set_ylabel(ylab)

        if tle != '':
            ax.set_title(tle)

        return dfPlot

    @staticmethod
    def mf_write_well(welfile_out, MXACTW, IWELCB, welData, frm=None):

        with open(welfile_out, 'w') as fw:
            fw.write('{:<10d}'.format(MXACTW) + ' ' + str(IWELCB).strip() + '          NOPRINT\n')
            iper = 0
            for p in welData:
                if frm is not None:
                    frm.setValue(frm.GetValue() + 1, 'Saving the Well file:')
                iper += 1
                fw.write('{:<10d}'.format(p.shape[0]) + '0         Stress Period ' + str(iper) + '\n')
#                print(p.values)
#                 np.savetxt(fw, p.values, fmt=['%10.0f']*4)
                for i, row in p.iterrows():
                    fw.write('{:<10.0f}'.format(row['layer']) + '{:<10.0f}'.format(row['row']) + '{:<10.0f}'.format(row['col']) + '{:<10.2f}\n'.format(row['rate']) )



    @staticmethod
    def mf_write_name(mfNam_file, mfName):
        '''
        write the MODFLOW name file
        '''
        #nam.to_csv(namefile, sep=' ', header=False, index=False)
        #print(mfName)
        with open(mfNam_file, 'w') as f:
            for i, p in mfName.iterrows():
                f.write('{:15s}{:5d}  {:s}\n'.format(p[0], int(p[1]), p[2]))

#%%
if __name__ == '__main__':
    from namefile import Namefile
    from zone import Zone
    name = Namefile(r'D:\UNW\gui-lite\unw-full\name-baseline.txt')
    os.chdir(name['MASTER'])
    zone = Zone(r'zones\nrd.zon', name)
    mf = Modflow(r'D:\UNW\gui-lite\unw-full\baseline\historical.nam', name)
#    m.mf_write_name(r'D:\UNW\gui-lite\data\modflow\baseline3yr_.nam')
    df = mf.zonbud(zone)
    df['Model'] = 'MMM'
    Modflow.zonplot(mf, df, selectedFlow=['Groundwater Pumping'], selectedZone=[],
                AggZone=False, AggVar=False, CumSum=False, plotType='bar', stacked=False, month=False)

#    z.mf_read_well()
#    z.welData

    mf.mf_write_name(r'D:\LPMT\SUSTAIN_GUI\baseline\test.nam', mf.name)
