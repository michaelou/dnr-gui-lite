# -*- coding: utf-8 -*-
"""
Created on Mon Aug 20 07:34:59 2018

@author: s-gou1
"""
import os
import pandas as pd
import numpy as np
from namefile import Namefile
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

class Rswb(Namefile):

    # long var names: short var name used in the model outputs
    varnames = {'Precipitation':'Precip',
                'Groundwater pumping': 'GW_Pumping',
                'Surface Delivery': 'SW_Del',
                'Streamflow': 'SF',
                'Direct DP': 'DP_dir',
                'Canal Seepage' : None,
                'Reinfiltration': 'RO_to_DP',
                'Direct ET': 'ET_dir',
                'ETtrans': 'ET_Transfer',
                'Surface Loss': 'SL',
                'Transmission Loss': 'RO_to_ET'}

    def __init__(self, namefile=None, namefile_path=None, historical=True):
        self.name = 'rswb class'
        if namefile_path is not None:
            super().__init__(namefile_path)
        if namefile is not None:
            for k,v in namefile.items():
                self[k] = v
        self.read_years(historical)


    def read_years(self, historical=True):
        os.chdir(self['MASTER'])
        self.years = pd.read_csv(self['YEARS'].split()[0]) if historical else pd.read_csv(self['YEARS'].split()[-1])


    def combine_columns(self):
        """
        combine some of the columns in the budget dataframe
        """
        vv = {v: k for k, v in self.varnames.items()}
        names = []
        df = self.df
        for c in df.columns:
            if c in vv.keys():
                names.append(vv[c])
            else:
                names.append(c)
        df.columns = names
        df['ET'] = df['Direct ET'] + df['ETtrans'] + \
                   df['Surface Loss'] + df['Transmission Loss']
        df['Recharge'] = df['Direct DP'] + df['Reinfiltration'] + \
                         0.0 if 'Canal Seepage' not in df.columns else df['Canal Seepage']



    def zonbud(self, zone, frmProgress=None, irri=None, crop=None):
        zarray = zone.zarray.flatten()
        # change the work dir
        os.chdir(self['MASTER'])
        # read the coefficients
        coef_zon = pd.read_csv(self['CELLLOC'], index_col=0, skipinitialspace=True)
        # exclude ZERO zone which are inactive areas
        coef_zon = coef_zon[coef_zon['CoefZone'] > 0]
        #coef_zon.loc[:,['Soil','CoefZone','ROZone']] = coef_zon.loc[:,['Soil','CoefZone','ROZone']].astype('uint8')   # change the index columns to integer type
        coef_val = pd.read_csv(self['COEFF'], index_col=['Zone','Soil'], skipinitialspace=True)
        coef_rov = pd.read_csv(self['COEFFRO'], index_col=0, skipinitialspace=True)

        #coef_zon['LossFactor'] = [0.5 if m2g == 0 else min(1- np.exp(-coef_rov.loc[iro, 1] * m2g), 1.0) for m2g, iro in zip(coef_zon['MitoGauge'], coef_zon['ROZone'])]
        coef_zon['LossFactor'] = coef_zon.apply(lambda x: 0.5 if x['MitoGauge'] < 1e-3 else min(1- np.exp(-coef_rov.loc[int(x['ROZone']), str(int(x['Soil']))] * x['MitoGauge']), 1.0), axis=1)
        coef_zon['PerToRchg'] = coef_val.loc[coef_zon[['CoefZone','Soil']].apply(tuple,axis=1).values, 'PerToRchg'].values

        df_all = []

        for i, yr in self.years.iterrows():
            if frmProgress is not None:
                if frmProgress.WasCancelled(): break
                frmProgress.setValue(i+1)
            
            if os.path.exists(os.path.join(self['OUTDIR'], 'RAW', 'RAW_WSPP' + str(yr[1]) + '.txt')):
                r = pd.read_csv(os.path.join(self['OUTDIR'], 'RAW', 'RAW_WSPP' + str(yr[1]) + '.txt'), skipinitialspace=True)
            else:
                r = pd.read_csv(os.path.join(self['OUTDIR'], 'RAW', 'RAW_WSPP' + str(yr[1]) + '.zip'), skipinitialspace=True)
            # eliminate the monthly values
            r = r[r['Month'] == 0]
            idx = r['Cell'].values
            r['SF'] = r['Runoff'] * (1-coef_zon.loc[idx,'LossFactor'].values)
            r['RO2ET'] = r['Runoff'] * coef_zon.loc[idx,'LossFactor'].values * (1 - coef_zon.loc[idx,'PerToRchg'].values)
            r['RO2DP'] = r['Runoff'] - r['SF'] - r['RO2ET']

            r['Zone'] = zarray[idx-1]

            r['Recharge'] = r['Deep_Percolation'] + r['RO2DP']
            r['Groundwater pumping'] = r['GW_Pumping']
            # This control what variable to be output
            raw = r[['Zone','Crop','Irr_Type', 'Acres', 'Groundwater pumping', 'Recharge']].groupby(['Zone','Crop','Irr_Type']).sum()
            raw['Year'] = yr[0]
            df_all.append(raw)

        dfall = pd.concat(df_all).reset_index()
        dfall.loc[:, 'Zone'] = np.array(zone.znames)[dfall['Zone'].astype(int)-1]
        if irri is not None:
            dfall.loc[:, 'Irr_Type'] = np.array(irri)[dfall['Irr_Type']-1]
        if crop is not None:
            dfall.loc[:, 'Crop'] = np.array(crop)[dfall['Crop']-1]
        self.df = dfall

    def zonplot(self, ax=None, selectedFlow=[], selectedIrr=[], selectedCrop=[], selectedZone=[],
                AggCrop=False, AggIrr=False, AggZone=False, CumSum=False, plotType='bar', stacked=False,
                xlbl='', ylbl='', tle=''):
        if ax is None:
            ax = plt.gca()

        df = self.df[['Year', 'Irr_Type', 'Crop', 'Zone'] + selectedFlow].copy()
        df.loc[:, 'Year'] = pd.DatetimeIndex(df['Year'].astype(str))
        if len(selectedIrr) > 0:
            df = df[[ir in selectedIrr for ir in df['Irr_Type']]]
        if len(selectedCrop) > 0:
            df = df[[ir in selectedCrop for ir in df['Crop']]]
        if len(selectedZone) > 0:
            df = df[[ir in selectedZone for ir in df['Zone']]]

        if AggCrop:
            df['Crop'] = ''
        if AggIrr:
            df['Irr_Type'] = ''
        if AggZone:
            df['Zone'] = ''

        dfs = df.groupby(['Year', 'Irr_Type', 'Crop', 'Zone'], as_index=False).sum()
        dfPlot = []

        for g, d in dfs.groupby(['Irr_Type', 'Crop', 'Zone']):
            lbls = [(f + ' ' + g[0] + ' ' + g[1] + ' ' + g[2]).strip() for f in selectedFlow]
            e = d.set_index('Year')[selectedFlow]
            if CumSum:
                e = e.cumsum()
            e.columns = lbls

            dfPlot.append(e)
        dfPlot = pd.concat(dfPlot, axis=1)

        if plotType == 'bar':
            dfPlot.index = dfPlot.index.year

        if plotType == 'line':
            dfPlot.plot(ax=ax, kind=plotType, legend=True, alpha=1.0, stacked=False)
        else:
            dfPlot.plot(ax=ax, kind=plotType, legend=True, alpha=0.7, stacked=stacked, linewidth=0)

        if xlbl != '':
            ax.set_xlabel(xlbl)
        else:
            ax.set_xlabel('Time')
#
#        years = mdates.YearLocator()   # every year
#        yearsFmt = mdates.DateFormatter('%Y')
#        ax.xaxis.set_major_locator(years)
#        ax.xaxis.set_major_formatter(yearsFmt)
        if ylbl != '':
            ax.set_ylabel(ylbl)
        else:
            if selectedFlow == ['Acres']:
                ax.set_ylabel('Area (acre)')
            else:
                ax.set_ylabel('Flow (acre-feet per year)')

        if tle != '':
            ax.set_title(tle)

        return dfPlot





#%%
def testNRD():
    from namefile import Namefile
    from zone import Zone
    # NRD Zone
    namefile = Namefile(r'D:\LPMT\SUSTAIN_GUI\name-baseline.txt')
    z = Zone(r'D:\LPMT\SUSTAIN_GUI\zones\nrd.zon', namefile)
    z.plot()
    r = Rswb(namefile)
    r.zonbud(z)
    print(r.df)

if __name__ == '__main__':
    from namefile import Namefile
    from zone import Zone
    import geopandas as gpd


    #
    name = Namefile(r'D:\UNW\gui-lite\unw-full\name-baseline.txt')
    gdf_grid = gpd.read_file(r'D:\UNW\gui-lite\unw-full\zones\UNW_WholeGrid.shp')
    zone = Zone(r'zones\polygonTest.shp', name, gdf_grid)
    ax = zone.plot(setaxis=False)
    r = Rswb(name)
    r.zonbud(zone, irri=r['IRRI'].split(), crop=r['CROP'].split())

    df = Rswb.zonplot(r, selectedFlow=['Recharge'], selectedIrr=['GW'], selectedCrop=['Corn'], selectedZone=[], plotType='bar')
    print(df)
