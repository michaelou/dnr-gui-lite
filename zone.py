# -*- coding: utf-8 -*-
"""
Created on Mon Aug 20 11:09:13 2018

@author: Michael Ou
"""
import os
import numpy as np
import geopandas as gpd
import matplotlib.pyplot as plt
from osgeo import gdal, ogr
from shapely.geometry import Polygon
import shutil
proj4_neb = '+proj=lcc +lat_1=40 +lat_2=43 +lat_0=39.83333333333334 +lon_0=-100 +x_0=500000.0000000002 +y_0=0 +ellps=GRS80 +datum=NAD83 +to_meter=0.3048006096012192 +no_defs'
class Zone:
    
    def __init__(self, inputZoneFile=None, ndim=None, gdf_grid=None, 
                 rowfield='Row', colfield='Column', zonfield='Name', 
                 outputZoneFile=None, inputZoneStr=None, llcord=None, cellsize=5280):
        """
        read the zones using the zone file or shapefiles
        0 is the inactive zone and will be ignored
        the first zone name (index 0 in znames) is 1 in the zone array
        ========= input =========
        inputZoneFile - the file path of the zone file
        """
#        os.chdir(namefile['MASTER'])
#        self.ndim = None
#        self.zarray = None
#        self.znames = None

        # generate the x, y coordinates
        
        if inputZoneStr is not None:
            # zone file from a uploader
            lines = inputZoneStr.split('\n')
            ws = lines[0].split('#')
            
            self.ndim = [int(s) for s in ws[0].split()]
            self.znames = np.array([z.strip() for z in ws[1].split(',')])
            self.nzone = len(self.znames)
            
            datalines = '\n'.join(lines[2:])
            self.zarray = np.fromstring(datalines, dtype=int, count=self.ndim[1]*self.ndim[2], sep=' ').reshape(self.ndim[1:])
            

        # generate the zone array
        elif inputZoneFile.lower().strip().endswith('.shp'):
        # make zones based on the shapefiles
            ndim = [int(s) for s in ndim.split()]
            if gdf_grid is not None:
                gdf_zone = gpd.read_file(inputZoneFile)
                gdf_zone.to_crs(gdf_grid.crs, inplace=True)
                if 'Polygon' in gdf_zone.geom_type.values:
                    #isPolygon = True
                    oldGeo = gdf_grid.geometry.copy()
                    gdf_grid.geometry = gdf_grid.geometry.centroid
    
                gdf_grid_zone = gpd.sjoin(gdf_grid, gdf_zone)
                znames = gdf_grid_zone[zonfield].unique()
                nzone = len(znames)
                
                zid = {z:(i+1) for z, i in zip(znames, range(nzone))}
                zarray = np.zeros(ndim[1:])
                zarray[gdf_grid_zone[rowfield] - 1, gdf_grid_zone[colfield] - 1] = [zid[z] for z in gdf_grid_zone[zonfield].values]
                gdf_grid.geometry = oldGeo
            else:
                bounds = llcord
                bounds.append(bounds[0] + ndim[2] * cellsize)
                bounds.append(bounds[1] + ndim[1] * cellsize)
                
                env = Polygon([[bounds[0], bounds[1]],
                               [bounds[2], bounds[1]],
                               [bounds[2], bounds[3]],
                               [bounds[0], bounds[3]]],)
                gdf_env = gpd.GeoDataFrame(geometry=[env])
                
                gdf_zone = gpd.read_file(inputZoneFile)
                gdf_zone.to_crs(crs=proj4_neb, inplace=True)
                gdf_zone = gpd.overlay(gdf_zone, gdf_env, how='intersection')
                gdf_zone = gdf_zone.dissolve(zonfield, as_index=False)

                nzone = gdf_zone.shape[0]
                gdf_zone['ZoneID'] = np.arange(1, nzone+1)
                
                try:
                    os.mkdir('tempZone')
                except:
                    print("An exception occurred when creating directory tempZone")
                
                gdf_zone.to_file('tempZone/temp.shp')
                zone_tif = gdal.Rasterize('tempZone/temp.tif','tempZone/temp.shp', attribute='ZoneID',
                               outputBounds=bounds, xRes=cellsize, yRes=cellsize)
                
                znames = gdf_zone[zonfield].values
                zarray = zone_tif.ReadAsArray()
                del zone_tif
                del gdf_zone

                shutil.rmtree('tempZone', True)
                
            self.znames = znames
            self.nzone = nzone
            self.zarray = zarray.astype(int)
            self.ndim = ndim
            if outputZoneFile is not None:
                self.zonefile = outputZoneFile
                self.writeZone(outputZoneFile)

        else:
        # read the zone file
            self.zonefile = inputZoneFile
            with open(inputZoneFile, 'r') as f:
                l = f.readline()
                ws = l.split('#')
                self.ndim = [int(s) for s in ws[0].split()]
                self.znames = np.array([z.strip() for z in ws[1].split(',')])
                self.nzone = len(self.znames)
                l = f.readline()
                self.zarray = np.fromfile(f, dtype=int, count=self.ndim[1]*self.ndim[2], sep=' ').reshape(self.ndim[1:])

    
    # lat lon and z
    @staticmethod
    def plotly_fig(zarray, znames, gdf_grid, rowfield='Row', colfield='Column', grid_size=0.05, epsg=4269, bounds=[-106, 39.5,-94, 43.5]):
        from shapely.geometry import Point
        from plotly import graph_objs as go
        # first dissolve the zones
        cm = plt.get_cmap('tab20', len(znames)) if len(znames) <= 20 else plt.get_cmap('jet', len(znames))    # discrete colors
        gdf_grid['iZone'] = zarray[gdf_grid[rowfield]-1, gdf_grid[colfield]-1]
        gdf_zone = gdf_grid.dissolve('iZone', as_index=False).to_crs(epsg=epsg)
        gdf_zone['Zone'] = znames[gdf_zone['iZone']-1]
        gdf_zone['Color'] = ['rgb' + str(tuple(c*255 for c in cm(iz-1)[:3])) for iz in gdf_zone['iZone']]
        
        # add the dots in Nebraska for hover events
        xx, yy = np.meshgrid(np.arange(bounds[0], bounds[2], grid_size), np.arange(bounds[1], bounds[3], grid_size))
        pnts = [Point(x, y) for x, y in zip(xx.flatten(), yy.flatten())]
        gs = gpd.GeoDataFrame(geometry=pnts)
        gs.crs = gdf_zone.crs
        gs = gpd.sjoin(gs, gdf_zone)
        
#        fig, ax = plt.subplots(dpi=300)
#        gs.plot(ax=ax)
#        gdf_zone.plot(ax=ax, column='Zone', alpha=0.5)
        
        trace_model = go.Scattermapbox(
            lon=[pt.x for pt in gs.geometry], 
            lat=[pt.y for pt in gs.geometry], 
            text=gs['Zone'], 
            hoverinfo='text', 
            opacity=0,
            mode='markers',
            showlegend=False,
            marker=dict(color=gs['Color'], )
        )

        
        # add the zones
        traces = [trace_model]
        for i, row in gdf_zone.iterrows():
            first=True
            for g in ([row.geometry] if row.geometry.type == 'Polygon' else row.geometry):
                xy = g.boundary.coords
                traces.append(go.Scattermapbox(
                    lon=[ixy[0] for ixy in xy],
                    lat=[ixy[1] for ixy in xy],
                    fill='toself',
                    opacity=0.5,
                    mode= 'lines',
                    line=dict(color=row['Color'],),
                    name=row['Zone'],
                    hoverinfo='name',
                    showlegend=first,
                ))
                first=False
            
        center = {'lon':gs.bounds['minx'].mean(), 'lat':gs.bounds['miny'].mean()}
        layout = go.Layout(dict(
                autosize=True,
#                width=1200,
                height=600,
                xaxis=dict(visible=True),
                margin=dict(l=5, t=30, b=5),
                mapbox=dict(accesstoken='pk.eyJ1IjoibHVja3ltaWNoYWVsIiwiYSI6ImNqcGtjMzUwZjAxdGQ0OG85NnlyNGN6c3oifQ.Kb2xt2FJfhjRerSAqVCTVA',
                          bearing=0,
                          center=center,
                          pitch=0,
                          zoom=6,
                          style='mapbox://styles/luckymichael/cjpkc4sqy084f2srrflsltscv',
                          ) 
                )
              )
        fig = dict(data=traces, layout=layout)
        return fig
        

    def plot(self, ax=None, setaxis=True, mask=None, legend=True, *args, **kwargs):
        if ax == None:
            ax = plt.gca()
        
        if 0 in self.zarray:
            nzone = self.nzone + 1
            zarray = np.where(self.zarray == 0, nzone, self.zarray)
            _0name = 'Unassigned' if 'Other' in self.znames else 'Other'
            znames = list(self.znames) + [_0name]
        else:
            nzone = self.nzone
            zarray = self.zarray
            znames = self.znames
            
        # apply Mask
        zarray = zarray.reshape(self.ndim[1:]) * (1.0 if mask is None else mask)    
        
        cmap = plt.get_cmap('Paired', nzone) if nzone <= 12 else plt.get_cmap('gist_stern', nzone)    # discrete colors
        axe= ax.pcolormesh(zarray, cmap=cmap, vmin = 0.5, vmax = nzone+0.5)
        if legend:
            t = plt.colorbar(axe, ax=ax, ticks=np.arange(nzone)+1, shrink=0.7)
            t.set_ticklabels(znames)
#
        ax.set_xlim(0, self.ndim[2])
        ax.set_ylim(self.ndim[1], 0)
        if setaxis:
            xticks = ax.get_xticks()
            yticks = ax.get_yticks()
            ax.set_xticklabels([ str(int(xt+1)) for xt in xticks])
            ax.set_yticklabels([ str(int(yt+1)) for yt in yticks])
            ax.set_xlabel('Column Number')
            ax.axes.set_ylabel('Row Number')

        return ax


    def writeZone(self, outputFile):
        """
        write zone file for SUSTAIN, used when defining zones with Shapefiles
        """
        fmt = ('{:<' + str(int(np.log10(self.nzone)) + 2) + 'd}') * self.ndim[-1] + '\n'
        with open(outputFile, 'w') as fw:
            fw.write(('{:<10d}'*3).format(*self.ndim) + '#         ' + ','.join(self.znames) + '\n')
            for i in range(self.ndim[0]):
                fw.write('INTERNAL () 1 -1\n')
                for ir in range(self.ndim[1]):
                    fw.write((fmt).format(*self.zarray[ir, :]))


    def writeWSZone(self, zoneArrFile, zoneNamFile):
        """
        write zone file for RSWB
        """
        if zoneArrFile != None:
            with open(zoneArrFile, 'w') as f:
                f.write('Cell,ZoneID\n')
                cell=0
                for z in self.zarray.flatten():
                    cell += 1
                    f.write(str(cell) + ',' + str(z*2-1) + '\n')

        if zoneNamFile != None:
            with open(zoneNamFile, 'w') as f:
                f.write('Name,ZoneID\n')
                z = 0
                for zn in self.znames:
                    z += 1
                    f.write(zn + ',' + str(z*2-1) + '\n')
#%%
def reproject_et(inpath, outpath, src_crs='EPSG:102704', dst_crs='EPSG:4269', cellsize=0.05):
    import rasterio
    from rasterio.warp import calculate_default_transform, reproject, Resampling
#    dst_crs = new_crs # CRS for web meractor 

    with rasterio.open(inpath) as src:
        transform, width, height = calculate_default_transform(
            src.crs, dst_crs, src.width, src.height, *src.bounds, resolution=(cellsize, cellsize))
        kwargs = src.meta.copy()
        kwargs.update({
            'crs': dst_crs,
            'transform': transform,
            'width': width,
            'height': height
        })

        with rasterio.open(outpath, 'w', **kwargs) as dst:
            reproject(
                source=src,
                destination=dst,
                src_transform=src.transform,
                src_crs=src.crs,
                dst_transform=transform,
                dst_crs=dst_crs,
                resampling=Resampling.nearest)
                
#%%
if __name__ == '__main__':
    from namefile import Namefile
    from plotly.offline import plot, iplot
    from osgeo import gdal
    
#    n = Namefile(r'D:\UNW\gui-lite\unw-full\name-baseline.txt')
#    z = Zone(r'zones\county.zon', n)
#    ax = z.plot(setaxis=False)
#    c = gpd.read_file(r'D:\@HydroDATA\Nebraska\StateHydrologyMap\City.shp')
#    z.transformShp(c)
#    c.plot(ax=ax)

#    namefile = Namefile(r'D:\Cloud\Dropbox\dnr\Watershed_Model_Development_Project\dash-salesforce-crm\data\unw\name-baseline.txt')
#    ndim = [int(i) for i in namefile['DIM'].split()]
#    bounds = [float(s) for s in namefile['LOWLEFT'].split()]
#    cellsize = float(namefile['CELLSIZE'])
#    bounds.append(bounds[0] + cellsize * ndim[2] * cellsize)
#    bounds.append(bounds[1] + cellsize * ndim[1] * cellsize)
#    
#    gdal.Rasterize(r'D:\Cloud\Dropbox\dnr\Watershed_Model_Development_Project\dash-salesforce-crm\temp\4e2caa8c-75a7-4187-ad7d-d7ae73cc3166\county.tif',
#                   r'J:\@HydroDATA\Nebraska\County Boundaries - TIGER 2010\county_bound.shp', outputBounds=bounds, xRes=5280, yRes=5280)

    #%% lpmt
    if False:
        namefile = Namefile(r'D:\LPMT\SUSTAIN_GUI\name-baseline.txt')
        gdf_grid = gpd.read_file('D:/LPMT/SUSTAIN_GUI/zones/PlatteGrid.shp', encoding="UTF-8")
        zone = Zone(r'J:\@HydroDATA\Nebraska\County Boundaries - TIGER 2010\county_bound.shp', ndim=namefile['DIM'], gdf_grid=gdf_grid, 
                 rowfield=namefile['SHPGRIDROW'], colfield=namefile['SHPGRIDCOL'], zonfield='NAME10')
        ax = Zone.plot(zone, setaxis=False, legend=True, dpi=300)
        
        
        zone = Zone(r'E:\@HydroDATA\Nebraska\Natural Resources District Boundary - Updated 2011-8\nrd_bound.shp', 
                    name, gdf_grid, zonfield='NRD_Name')
        
        ax = Zone.plot(zone, setaxis=False, legend=True)
        fig = zone.plotly_fig(gdf_grid=gdf_grid, rowfield=namefile['SHPGRIDROW'], colfield=namefile['SHPGRIDCOL'])
    
    if False:
        # test NRD
        namefile = Namefile(r'D:\UNW\gui-lite\unw-full\name-baseline.txt')
        os.chdir(namefile['MASTER'])
        z = Zone(r'zones\nrd.zon')
        ax = zone.plot(setaxis=False)
    
    
        # test shapefile
        namefile = Namefile(r'D:\UNW\gui-lite\unw-full\name-baseline.txt')
        gdf_grid = gpd.read_file(r'D:\UNW\gui-lite\unw-full\zones\UNW_WholeGrid.shp')
#        gdf_zone = gpd.read_file(r'D:\UNW\gui-lite\unw-full\zones\polygonTest.shp')
#    zone = Zone(r'E:\@HydroDATA\Nebraska\County Boundaries - TIGER 2010\county_bound.shp', name, gdf_grid, zonfield='NAME10')
#%%
        def read_shp():
            gdf_grid = gpd.read_file(r'D:\UNW\gui-lite\unw-full\zones\UNW_WholeGrid.shp')
            zone = Zone(inputZoneFile=r'J:\@HydroDATA\Nebraska\County Boundaries - TIGER 2010\county_bound.shp', ndim=namefile['DIM'], gdf_grid=gdf_grid, 
                        rowfield=namefile['SHPGRIDROW'], colfield=namefile['SHPGRIDCOL'], zonfield='NAME10', outputZoneFile=r'D:\UNW\gui-lite\unw-full\zones\county.shp')

#%%
        def read_shp1():
            zone = Zone(inputZoneFile=r'J:\@HydroDATA\Nebraska\County Boundaries - TIGER 2010\county_bound.shp', ndim=namefile['DIM'], zonfield='NAME10',
                        llcord=[float(c) for c in namefile['LOWLEFT'].split()], cellsize=5280, outputZoneFile=r'D:\UNW\gui-lite\unw-full\zones\county.zon')

        zone = Zone(inputZoneFile=r'D:\UNW\gui-lite\unw-full\zones\polygonTest.shp', ndim=namefile['DIM'], zonfield='name',
                                llcord=[float(c) for c in namefile['LOWLEFT'].split()], cellsize=5280, outputZoneFile=r'D:\UNW\gui-lite\unw-full\zones\test.zon')
        zone.plot()
        
        
        zone = Zone(inputZoneFile=r'J:\@HydroDATA\Nebraska\Natural Resources District Boundary - Updated 2011-8\nrd_bound.shp', ndim=namefile['DIM'], zonfield='NRD_Name',
                                llcord=[float(c) for c in namefile['LOWLEFT'].split()], cellsize=5280, outputZoneFile=r'D:\UNW\gui-lite\unw-full\zones\nrd.zon')
        zone.plot()
        zone.zarray
