# -*- coding: utf-8 -*-
"""
Created on Tue Sep 11 09:43:24 2018

@author: s-gou1
"""

import wx
import wx.lib.agw.pyprogress
import wx.lib.agw.pybusyinfo

class frameProgress(wx.ProgressDialog):
    def __init__(self, title, message, maximum=100, parent=None):
        self.msg = message
        msg = self.msg + '; 0/' + str(maximum)
        super().__init__(title, msg, maximum, parent=parent, style=wx.PD_AUTO_HIDE|wx.PD_SMOOTH|wx.PD_CAN_ABORT|wx.STAY_ON_TOP)
        self.ShowModal()

    def setValue(self, v, msg=None):
        if msg is not None:
            self.msg = msg
        self.Update(v, self.msg + ' ' + str(v) + '/' + str(self.GetRange()))

class frameRunning(wx.lib.agw.pyprogress.PyProgress):
    def __init__(self, msg, parent=None):

        super().__init__(parent=parent, title='Busy', message=msg, agwStyle=wx.PD_APP_MODAL)

        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.setValue, self.timer)
        self.val = 0
        self.ShowModal()
        self.timer.Start(2000)

    def setValue(self, event):
        print(self.val)
        self.val += 1
        self.UpdatePulse()


class frameBusy(wx.lib.agw.pybusyinfo.PyBusyInfo):
    def __init__(self, msg, parent=None):

        super().__init__(parent=parent, title='Busy', message=msg)

        self.Show()