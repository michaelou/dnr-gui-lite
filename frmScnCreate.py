# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Sep  7 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class frmScnCreate
###########################################################################

class frmScnCreate ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Create Scenario: Recharge/Pumping Change", pos = wx.DefaultPosition, size = wx.Size( 1026,632 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_APPWORKSPACE ) )

		self.m_toolBar1 = self.CreateToolBar( wx.TB_HORIZONTAL|wx.TB_TEXT, wx.ID_ANY )
		self.m_tool1 = self.m_toolBar1.AddLabelTool( wx.ID_ANY, u"Add", wx.ArtProvider.GetBitmap( wx.ART_ADD_BOOKMARK,  ), wx.NullBitmap, wx.ITEM_NORMAL, u"Open a name file", u"Open a name file", None )

		self.m_tool2 = self.m_toolBar1.AddLabelTool( wx.ID_ANY, u"Delete", wx.ArtProvider.GetBitmap( wx.ART_DEL_BOOKMARK,  ), wx.NullBitmap, wx.ITEM_NORMAL, u"Save the current name file", u"Save the current name file", None )

		self.m_tool21 = self.m_toolBar1.AddLabelTool( wx.ID_ANY, u"Save", wx.ArtProvider.GetBitmap( wx.ART_FILE_SAVE_AS,  ), wx.NullBitmap, wx.ITEM_NORMAL, u"Save to a name file", u"Save to a name file", None )

		self.m_tool11 = self.m_toolBar1.AddLabelTool( wx.ID_ANY, u"Reset", wx.ArtProvider.GetBitmap( wx.ART_REDO,  ), wx.NullBitmap, wx.ITEM_NORMAL, u"Open a name file", u"Open a name file", None )

		self.m_tool22 = self.m_toolBar1.AddLabelTool( wx.ID_ANY, u"Run", wx.ArtProvider.GetBitmap( wx.ART_EXECUTABLE_FILE,  ), wx.NullBitmap, wx.ITEM_NORMAL, u"Save the current name file", u"Save the current name file", None )

		self.m_tool221 = self.m_toolBar1.AddLabelTool( wx.ID_ANY, u"Help", wx.ArtProvider.GetBitmap( wx.ART_HELP,  ), wx.NullBitmap, wx.ITEM_NORMAL, u"Show help", u"Show help", None )

		self.m_toolBar1.Realize()

		bSizer86 = wx.BoxSizer( wx.VERTICAL )

		bSizer87 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer88 = wx.BoxSizer( wx.HORIZONTAL )

		wxChkZoneChoices = [ u"County", u"NRD", u"Customized" ]
		self.wxChkZone = wx.RadioBox( self, wx.ID_ANY, u"Input Zone", wx.DefaultPosition, wx.DefaultSize, wxChkZoneChoices, 1, wx.RA_SPECIFY_ROWS )
		self.wxChkZone.SetSelection( 0 )
		bSizer88.Add( self.wxChkZone, 0, wx.ALL, 5 )

		self.m_button12 = wx.Button( self, wx.ID_ANY, u"Show Input Zones", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer88.Add( self.m_button12, 0, wx.ALIGN_RIGHT|wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer87.Add( bSizer88, 1, wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 5 )

		bSizer89 = wx.BoxSizer( wx.HORIZONTAL )

		wxChkPeriodChoices = [ u"Historical", u"Future 25 yr", u"Future 50 yr" ]
		self.wxChkPeriod = wx.RadioBox( self, wx.ID_ANY, u"Simulation Period", wx.DefaultPosition, wx.DefaultSize, wxChkPeriodChoices, 1, wx.RA_SPECIFY_ROWS )
		self.wxChkPeriod.SetSelection( 2 )
		self.wxChkPeriod.Hide()

		bSizer89.Add( self.wxChkPeriod, 0, wx.ALL, 5 )


		bSizer87.Add( bSizer89, 1, wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer86.Add( bSizer87, 0, wx.EXPAND, 5 )

		bSizer90 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer91 = wx.BoxSizer( wx.VERTICAL )

		wxClbFromZoneChoices = []
		self.wxClbFromZone = wx.CheckListBox( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wxClbFromZoneChoices, wx.LB_SINGLE )
		bSizer91.Add( self.wxClbFromZone, 1, wx.ALL|wx.EXPAND, 5 )

		bSizer92 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer93 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText3012 = wx.StaticText( self, wx.ID_ANY, u"Start Year", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3012.Wrap( -1 )

		bSizer93.Add( self.m_staticText3012, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.wxSpinStartYear = wx.SpinCtrl( self, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 65,-1 ), wx.SP_ARROW_KEYS, 1900, 2100, 0 )
		bSizer93.Add( self.wxSpinStartYear, 1, wx.ALL, 5 )


		bSizer92.Add( bSizer93, 0, wx.EXPAND, 5 )

		bSizer94 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText301 = wx.StaticText( self, wx.ID_ANY, u"End Year", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText301.Wrap( -1 )

		bSizer94.Add( self.m_staticText301, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.wxSpinEndYear = wx.SpinCtrl( self, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 65,-1 ), wx.SP_ARROW_KEYS, 1900, 2100, 0 )
		bSizer94.Add( self.wxSpinEndYear, 0, wx.ALL, 5 )


		bSizer92.Add( bSizer94, 0, wx.EXPAND, 5 )


		bSizer91.Add( bSizer92, 0, wx.EXPAND, 5 )

		wxClbMonthChoices = [u"Janunary", u"Feburay", u"March", u"April", u"May", u"June", u"July", u"August", u"September", u"October", u"November", u"December"]
		self.wxClbMonth = wx.CheckListBox( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wxClbMonthChoices, 0 )
		bSizer91.Add( self.wxClbMonth, 1, wx.ALL|wx.EXPAND, 5 )

		bSizer95 = wx.BoxSizer( wx.VERTICAL )

		bSizer96 = wx.BoxSizer( wx.VERTICAL )

		bSizer112 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText40 = wx.StaticText( self, wx.ID_ANY, u"Change existing wells:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText40.Wrap( -1 )

		bSizer112.Add( self.m_staticText40, 0, wx.ALL, 5 )

		bSizer97 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText41 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 10,-1 ), 0 )
		self.m_staticText41.Wrap( -1 )

		bSizer97.Add( self.m_staticText41, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.wxCheckPercent = wx.RadioButton( self, wx.ID_ANY, u"By percent (%)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.wxCheckPercent.SetMinSize( wx.Size( 130,-1 ) )
		self.wxCheckPercent.SetMaxSize( wx.Size( 130,-1 ) )

		bSizer97.Add( self.wxCheckPercent, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.wxTxtWellPercent = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.wxTxtWellPercent.SetMaxLength( 0 )
		self.wxTxtWellPercent.Enable( False )

		bSizer97.Add( self.wxTxtWellPercent, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer112.Add( bSizer97, 1, wx.EXPAND, 5 )

		bSizer98 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText411 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 10,-1 ), 0 )
		self.m_staticText411.Wrap( -1 )

		bSizer98.Add( self.m_staticText411, 0, wx.ALL, 5 )

		self.wxCheckAf = wx.RadioButton( self, wx.ID_ANY, u"By volume (af/yr)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.wxCheckAf.SetMinSize( wx.Size( 130,-1 ) )
		self.wxCheckAf.SetMaxSize( wx.Size( 130,-1 ) )

		bSizer98.Add( self.wxCheckAf, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.wxTxtWellAf = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.wxTxtWellAf.SetMaxLength( 0 )
		self.wxTxtWellAf.Enable( False )

		bSizer98.Add( self.wxTxtWellAf, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer112.Add( bSizer98, 1, wx.EXPAND, 5 )


		bSizer96.Add( bSizer112, 0, wx.EXPAND, 5 )

		bSizer113 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText401 = wx.StaticText( self, wx.ID_ANY, u"Add new:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText401.Wrap( -1 )

		bSizer113.Add( self.m_staticText401, 0, wx.ALL, 5 )

		bSizer99 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText412 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 10,-1 ), 0 )
		self.m_staticText412.Wrap( -1 )

		bSizer99.Add( self.m_staticText412, 0, wx.ALL, 5 )

		self.wxCheckNew = wx.RadioButton( self, wx.ID_ANY, u"Pumping (af/yr)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.wxCheckNew.SetMinSize( wx.Size( 130,-1 ) )
		self.wxCheckNew.SetMaxSize( wx.Size( 130,-1 ) )

		bSizer99.Add( self.wxCheckNew, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.wxTxtNewWel = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.wxTxtNewWel.SetMaxLength( 0 )
		self.wxTxtNewWel.Enable( False )

		bSizer99.Add( self.wxTxtNewWel, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer113.Add( bSizer99, 1, wx.EXPAND, 5 )

		bSizer991 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText413 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 10,-1 ), 0 )
		self.m_staticText413.Wrap( -1 )

		bSizer991.Add( self.m_staticText413, 0, wx.ALL, 5 )

		self.wxCheckNewRch = wx.RadioButton( self, wx.ID_ANY, u"Recharge (af/yr)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.wxCheckNewRch.SetMinSize( wx.Size( 130,-1 ) )
		self.wxCheckNewRch.SetMaxSize( wx.Size( 130,-1 ) )

		bSizer991.Add( self.wxCheckNewRch, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.wxTxtNewRch = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.wxTxtNewRch.SetMaxLength( 0 )
		self.wxTxtNewRch.Enable( False )

		bSizer991.Add( self.wxTxtNewRch, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer113.Add( bSizer991, 1, wx.EXPAND, 5 )


		bSizer96.Add( bSizer113, 0, wx.EXPAND, 5 )


		bSizer95.Add( bSizer96, 0, wx.EXPAND, 5 )


		bSizer91.Add( bSizer95, 0, wx.EXPAND, 5 )


		bSizer90.Add( bSizer91, 1, wx.EXPAND, 5 )

		bSizer100 = wx.BoxSizer( wx.VERTICAL )

		self.wxLstChange = wx.ListCtrl( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_REPORT|wx.LC_VRULES )
		bSizer100.Add( self.wxLstChange, 0, wx.ALL|wx.EXPAND, 5 )


		bSizer90.Add( bSizer100, 5, wx.EXPAND, 5 )


		bSizer86.Add( bSizer90, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer86 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_ACTIVATE, self.initFrm )
		self.Bind( wx.EVT_TOOL, self.addChange, id = self.m_tool1.GetId() )
		self.Bind( wx.EVT_TOOL, self.deleteChange, id = self.m_tool2.GetId() )
		self.Bind( wx.EVT_TOOL, self.saveFile, id = self.m_tool21.GetId() )
		self.Bind( wx.EVT_TOOL, self.resetChange, id = self.m_tool11.GetId() )
		self.Bind( wx.EVT_TOOL, self.runModel, id = self.m_tool22.GetId() )
		self.Bind( wx.EVT_TOOL, self.showHelp, id = self.m_tool221.GetId() )
		self.wxChkZone.Bind( wx.EVT_RADIOBOX, self.changeZone )
		self.m_button12.Bind( wx.EVT_BUTTON, self.showZone )
		self.wxChkPeriod.Bind( wx.EVT_RADIOBOX, self.changePeriod )
		self.wxCheckPercent.Bind( wx.EVT_RADIOBUTTON, self.checkPercent )
		self.wxCheckAf.Bind( wx.EVT_RADIOBUTTON, self.checkAf )
		self.wxCheckNew.Bind( wx.EVT_RADIOBUTTON, self.checkNew )
		self.wxCheckNewRch.Bind( wx.EVT_RADIOBUTTON, self.checkNew )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def initFrm( self, event ):
		event.Skip()

	def addChange( self, event ):
		event.Skip()

	def deleteChange( self, event ):
		event.Skip()

	def saveFile( self, event ):
		event.Skip()

	def resetChange( self, event ):
		event.Skip()

	def runModel( self, event ):
		event.Skip()

	def showHelp( self, event ):
		event.Skip()

	def changeZone( self, event ):
		event.Skip()

	def showZone( self, event ):
		event.Skip()

	def changePeriod( self, event ):
		event.Skip()

	def checkPercent( self, event ):
		event.Skip()

	def checkAf( self, event ):
		event.Skip()

	def checkNew( self, event ):
		event.Skip()



