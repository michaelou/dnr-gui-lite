# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Sep  7 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class frmWatshedBud
###########################################################################

class frmWatshedBud ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Watershed Model Data", pos = wx.DefaultPosition, size = wx.Size( 1070,723 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.Size( 959,720 ), wx.DefaultSize )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )

		bSizer78 = wx.BoxSizer( wx.VERTICAL )

		bSizer80 = wx.BoxSizer( wx.VERTICAL )

		bSizer497 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer54 = wx.BoxSizer( wx.VERTICAL )

		bSizer801 = wx.BoxSizer( wx.VERTICAL )

		bSizer1132 = wx.BoxSizer( wx.VERTICAL )

		wxChkZoneChoices = [ u"County", u"NRD", u"Customized" ]
		self.wxChkZone = wx.RadioBox( self, wx.ID_ANY, u"Result Summary Zone", wx.DefaultPosition, wx.DefaultSize, wxChkZoneChoices, 1, wx.RA_SPECIFY_ROWS )
		self.wxChkZone.SetSelection( 0 )
		bSizer1132.Add( self.wxChkZone, 0, wx.ALL|wx.EXPAND, 5 )

		bSizer45 = wx.BoxSizer( wx.VERTICAL )


		bSizer1132.Add( bSizer45, 1, wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer801.Add( bSizer1132, 0, wx.EXPAND, 5 )


		bSizer54.Add( bSizer801, 1, wx.EXPAND, 5 )

		bSizer48 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button12 = wx.Button( self, wx.ID_ANY, u"Show Zones", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer48.Add( self.m_button12, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_button121 = wx.Button( self, wx.ID_ANY, u"Load Results", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer48.Add( self.m_button121, 1, wx.ALL|wx.EXPAND, 5 )


		bSizer54.Add( bSizer48, 0, wx.EXPAND, 5 )

		bSizer53 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer50 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText301 = wx.StaticText( self, wx.ID_ANY, u"Variables:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText301.Wrap( -1 )

		bSizer50.Add( self.m_staticText301, 0, wx.ALL, 5 )

		self.wxTrcPlotVar = wx.TreeCtrl( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TR_DEFAULT_STYLE|wx.TR_HIDE_ROOT|wx.TR_MULTIPLE|wx.TR_ROW_LINES )
		self.wxTrcPlotVar.SetMinSize( wx.Size( -1,250 ) )

		bSizer50.Add( self.wxTrcPlotVar, 2, wx.ALL|wx.EXPAND, 5 )


		bSizer53.Add( bSizer50, 1, wx.EXPAND, 3 )

		bSizer52 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText30 = wx.StaticText( self, wx.ID_ANY, u"Zones:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText30.Wrap( -1 )

		bSizer52.Add( self.m_staticText30, 0, wx.ALL, 5 )

		wxClbPlotZoneChoices = []
		self.wxClbPlotZone = wx.CheckListBox( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wxClbPlotZoneChoices, 0 )
		self.wxClbPlotZone.SetMinSize( wx.Size( -1,90 ) )

		bSizer52.Add( self.wxClbPlotZone, 3, wx.ALL|wx.EXPAND, 5 )

		self.m_staticText3011 = wx.StaticText( self, wx.ID_ANY, u"Crops:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3011.Wrap( -1 )

		bSizer52.Add( self.m_staticText3011, 0, wx.ALL, 5 )

		wxClbPlotCropChoices = []
		self.wxClbPlotCrop = wx.CheckListBox( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wxClbPlotCropChoices, 0 )
		self.wxClbPlotCrop.SetMinSize( wx.Size( -1,90 ) )

		bSizer52.Add( self.wxClbPlotCrop, 3, wx.ALL|wx.EXPAND, 5 )

		self.m_staticText30111 = wx.StaticText( self, wx.ID_ANY, u"Irrigation:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText30111.Wrap( -1 )

		bSizer52.Add( self.m_staticText30111, 0, wx.ALL, 5 )

		wxClbPlotIrrChoices = []
		self.wxClbPlotIrr = wx.CheckListBox( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wxClbPlotIrrChoices, 0 )
		self.wxClbPlotIrr.SetMinSize( wx.Size( -1,60 ) )

		bSizer52.Add( self.wxClbPlotIrr, 2, wx.ALL|wx.EXPAND, 5 )


		bSizer53.Add( bSizer52, 1, wx.EXPAND, 5 )


		bSizer54.Add( bSizer53, 10, wx.EXPAND, 10 )

		bSizer57 = wx.BoxSizer( wx.VERTICAL )

		bSizer71 = wx.BoxSizer( wx.HORIZONTAL )

		wxChiPlotTypeChoices = [ u"Bar", u"Area", u"Line" ]
		self.wxChiPlotType = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wxChiPlotTypeChoices, 0 )
		self.wxChiPlotType.SetSelection( 0 )
		self.wxChiPlotType.SetMinSize( wx.Size( -1,25 ) )

		bSizer71.Add( self.wxChiPlotType, 1, wx.ALIGN_CENTER|wx.ALL|wx.EXPAND, 5 )

		self.wxChkStacked = wx.CheckBox( self, wx.ID_ANY, u"Stacked", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer71.Add( self.wxChkStacked, 1, wx.ALIGN_CENTER|wx.ALL|wx.EXPAND, 5 )

		self.wxChkCum = wx.CheckBox( self, wx.ID_ANY, u"Cumulative", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer71.Add( self.wxChkCum, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )


		bSizer57.Add( bSizer71, 0, wx.EXPAND, 5 )

		bSizer711 = wx.BoxSizer( wx.HORIZONTAL )

		self.wxChkAggZone = wx.CheckBox( self, wx.ID_ANY, u"Sum Zones", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer711.Add( self.wxChkAggZone, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.EXPAND, 5 )

		self.wxChkAggCrop = wx.CheckBox( self, wx.ID_ANY, u"Sum Crops", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.wxChkAggCrop.SetMinSize( wx.Size( -1,25 ) )

		bSizer711.Add( self.wxChkAggCrop, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )

		self.wxChkAggIrr = wx.CheckBox( self, wx.ID_ANY, u"Sum IrriTypes", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer711.Add( self.wxChkAggIrr, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )


		bSizer57.Add( bSizer711, 0, wx.EXPAND, 5 )


		bSizer54.Add( bSizer57, 0, wx.EXPAND, 1 )

		bSizer601 = wx.BoxSizer( wx.VERTICAL )

		bSizer59 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText34 = wx.StaticText( self, wx.ID_ANY, u"xAxis Label", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText34.Wrap( -1 )

		self.m_staticText34.SetMinSize( wx.Size( 60,-1 ) )

		bSizer59.Add( self.m_staticText34, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.wxTxtXLabel = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer59.Add( self.wxTxtXLabel, 1, wx.ALIGN_CENTER|wx.ALL, 5 )


		bSizer601.Add( bSizer59, 1, wx.EXPAND, 5 )

		bSizer591 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText341 = wx.StaticText( self, wx.ID_ANY, u"yAxis Label", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText341.Wrap( -1 )

		self.m_staticText341.SetMinSize( wx.Size( 60,-1 ) )

		bSizer591.Add( self.m_staticText341, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.wxTxtYLabel = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer591.Add( self.wxTxtYLabel, 1, wx.ALIGN_CENTER|wx.ALL, 5 )


		bSizer601.Add( bSizer591, 1, wx.EXPAND, 5 )

		bSizer592 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText342 = wx.StaticText( self, wx.ID_ANY, u"Plot Title", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText342.Wrap( -1 )

		self.m_staticText342.SetMinSize( wx.Size( 60,-1 ) )

		bSizer592.Add( self.m_staticText342, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.wxTxtPlotTitle = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer592.Add( self.wxTxtPlotTitle, 1, wx.ALIGN_CENTER|wx.ALL, 5 )


		bSizer601.Add( bSizer592, 1, wx.EXPAND, 5 )


		bSizer54.Add( bSizer601, 0, wx.EXPAND, 3 )

		bSizer101 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button10 = wx.Button( self, wx.ID_ANY, u"Plot", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer101.Add( self.m_button10, 1, wx.ALL, 5 )

		self.m_button11 = wx.Button( self, wx.ID_ANY, u"Export Data", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer101.Add( self.m_button11, 1, wx.ALL, 5 )


		bSizer54.Add( bSizer101, 0, wx.EXPAND, 5 )


		bSizer497.Add( bSizer54, 0, wx.EXPAND, 5 )

		wxSizPlot = wx.BoxSizer( wx.VERTICAL )

		self.wxPnlPlot = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		wxSizPlot.Add( self.wxPnlPlot, 1, wx.EXPAND |wx.ALL, 5 )


		bSizer497.Add( wxSizPlot, 4, wx.EXPAND, 5 )


		bSizer80.Add( bSizer497, 1, wx.EXPAND, 5 )


		bSizer78.Add( bSizer80, 15, wx.EXPAND, 5 )


		self.SetSizer( bSizer78 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_ACTIVATE, self.initFrm )
		self.wxChkZone.Bind( wx.EVT_RADIOBOX, self.changeZone )
		self.m_button12.Bind( wx.EVT_BUTTON, self.showZone )
		self.m_button121.Bind( wx.EVT_BUTTON, self.loadResults )
		self.wxChiPlotType.Bind( wx.EVT_CHOICE, self.plotTypeChange )
		self.m_button10.Bind( wx.EVT_BUTTON, self.makePlot )
		self.m_button11.Bind( wx.EVT_BUTTON, self.exportData )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def initFrm( self, event ):
		event.Skip()

	def changeZone( self, event ):
		event.Skip()

	def showZone( self, event ):
		event.Skip()

	def loadResults( self, event ):
		event.Skip()

	def plotTypeChange( self, event ):
		event.Skip()

	def makePlot( self, event ):
		event.Skip()

	def exportData( self, event ):
		event.Skip()


