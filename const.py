# -*- coding: utf-8 -*-
"""
Created on Tue Aug 21 06:41:40 2018

@author: Michael Ou
"""
import pandas as pd
import numpy as np

ifDebug = True

varnames = {'Precipitation':'Precip',
            'Groundwater pumping': 'GW_Pumping',
            'Surface Delivery': 'SW_Del',
            'Streamflow': 'SF',
            'Direct DP': 'DP_dir',
            'Canal Seepage' : None,
            'Reinfiltration': 'RO_to_DP',
            'Direct ET': 'ET_dir',
            'ETtrans': 'ET_Transfer',
            'Surface Loss': 'SL',
            'Transmission Loss': 'RO_to_ET'}

MF_FLOWS = pd.DataFrame({'Sign': [-1, 1, -1, 1, -1, -1, 1, 1, -1, 1, -1],
                         'Name': ['Storage Change', 'Constant Head Boundary', 'Groundwater Pumping', 'Drains',
                                  'Baseflow (River)', 'Groundwater ET', 'General Head Boundary', 'Groundwater Recharge',
                                  'Baseflow (Stream)', 'Groundwater Inflow', 'Groundwater Outflow']},
                        index = ['STORAGE', 'CONSTANT HEAD', 'WELLS', 'DRAINS',
                                 'RIVER LEAKAGE', 'ET', 'HEAD DEP BOUNDS', 'RECHARGE',
                                 'STREAM LEAKAGE', 'From Other Zones', 'To Other Zones'])
ACRE2FOOT = 43560
DAYS_IN_MONTH = np.array([31, 28.25, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31])


APP_NAME = 'SUSTAIN'
VERSION = '0.1.190512'

MODEL = 'UNW'

# wx constant