# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Aug 29 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class frmConfig
###########################################################################

class frmConfig ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Configuration", pos = wx.DefaultPosition, size = wx.Size( 690,168 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer107 = wx.BoxSizer( wx.VERTICAL )

		bSizer112 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText91 = wx.StaticText( self, wx.ID_ANY, u"Baseline Name File", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText91.Wrap( -1 )

		self.m_staticText91.SetMinSize( wx.Size( 110,15 ) )

		bSizer112.Add( self.m_staticText91, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.wxFilName = wx.FilePickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.*", wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE )
		bSizer112.Add( self.wxFilName, 5, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer107.Add( bSizer112, 1, wx.EXPAND, 5 )

		bSizer1122 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText911 = wx.StaticText( self, wx.ID_ANY, u"Scenario Output Dir", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText911.Wrap( -1 )

		self.m_staticText911.SetMinSize( wx.Size( 110,15 ) )

		bSizer1122.Add( self.m_staticText911, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.wxDirMaster = wx.DirPickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.DefaultPosition, wx.DefaultSize, wx.DIRP_DEFAULT_STYLE )
		bSizer1122.Add( self.wxDirMaster, 9, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer107.Add( bSizer1122, 1, wx.EXPAND, 5 )

		m_sdbSizer1 = wx.StdDialogButtonSizer()
		self.m_sdbSizer1OK = wx.Button( self, wx.ID_OK )
		m_sdbSizer1.AddButton( self.m_sdbSizer1OK )
		self.m_sdbSizer1Cancel = wx.Button( self, wx.ID_CANCEL )
		m_sdbSizer1.AddButton( self.m_sdbSizer1Cancel )
		self.m_sdbSizer1Help = wx.Button( self, wx.ID_HELP )
		m_sdbSizer1.AddButton( self.m_sdbSizer1Help )
		m_sdbSizer1.Realize();

		bSizer107.Add( m_sdbSizer1, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer107 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.quitMain )
		self.wxFilName.Bind( wx.EVT_FILEPICKER_CHANGED, self.changeNameFile )
		self.wxDirMaster.Bind( wx.EVT_DIRPICKER_CHANGED, self.changeOutDir )
		self.m_sdbSizer1Help.Bind( wx.EVT_BUTTON, self.gotoHelp )
		self.m_sdbSizer1OK.Bind( wx.EVT_BUTTON, self.confirmConfig )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def quitMain( self, event ):
		event.Skip()

	def changeNameFile( self, event ):
		event.Skip()

	def changeOutDir( self, event ):
		event.Skip()

	def gotoHelp( self, event ):
		event.Skip()

	def confirmConfig( self, event ):
		event.Skip()


