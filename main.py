import matplotlib
matplotlib.use("WxAgg")
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx #, wxc
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

#plt.style.use('ggplot')

# deal with the pyinstaller error #
#from pyproj import _datadir, datadir
#from fiona import _shim, schema
# deal with the pyinstaller error #

import sys
import os
import logging
import wx
import wx.xrc
import wx.adv



import subprocess
import pandas as pd
import geopandas as gpd
import numpy as np
import time
from flopy.utils import HeadFile
import webbrowser
from shapely.affinity import affine_transform


# other frames
from frmMain import frmMain
from frmConfig import frmConfig
from frmScnCreate import frmScnCreate
from frmGwLevel import frmGwLevel
from frmWatshedBud import frmWatshedBud
from frmModflowBud import frmModflowBud
from frameProgress import frameBusy, frameProgress

# other classes
from namefile import Namefile
from zone import Zone
from modflow import Modflow
from rswb import Rswb
from const import APP_NAME, VERSION, ACRE2FOOT, DAYS_IN_MONTH

logfile = time.strftime('%Y-%m-%d_%H_%M_%S') + '.log'
logging.basicConfig(filename=logfile, format='%(asctime)s \n%(message)s \n\n', level=logging.DEBUG)

IS_GTK = 'wxGTK' in wx.PlatformInfo
IS_WIN = 'wxMSW' in wx.PlatformInfo

class config:
    outdir = None
    missingNameWarn = True
    labelAlpha = 0.7 # used for geo reference

    def loadNamefile(namefile_path):
        config.name = Namefile(namefile_path)
        config.ndim = [int(i) for i in config.name['DIM'].split()]

        os.chdir(config.name['MASTER'])
        config.mask = np.fromfile(config.name['MASK'].split()[0].strip(), sep=' ').reshape(config.ndim[1:])
        config.mask = np.where(config.mask==0, np.nan, 1)
        #config.mf_his_bas = Modflow(config.name['MFNAM'].split()[0], config.name)
        if os.path.exists(config.name['SHPGRID']):
            config.gdf_grid = gpd.read_file(config.name['SHPGRID'])
        else:
            logging.exception('SHPGRID file not found.')
        if 'SHPGEOREF' in config.name.keys():
            if os.path.exists(config.name['SHPGEOREF']):
                config.gdf_georef = gpd.read_file(config.name['SHPGEOREF'])
                bounds = config.gdf_grid.geometry.bounds
                coors = bounds.agg({'minx':'min', 'miny':'min', 'maxx':'max', 'maxy':'max'})
                xscale = config.ndim[2] / (coors['maxx'] - coors['minx'])
                yscale = -config.ndim[1] / (coors['maxy'] - coors['miny'])
                xoff = -coors['minx'] * xscale
                yoff = -coors['maxy'] * yscale
                geo = []
                matrix = [xscale, 0.0, 0.0, yscale, xoff, yoff]

                config.gdf_georef.to_crs(config.gdf_grid.crs, inplace=True)
                for g in config.gdf_georef.geometry:
                    geo.append(affine_transform(g, matrix))

                config.gdf_georef.geometry = geo
            else:
                logging.exception('SHPGEOREF file not found.')
        else:
            logging.exception('SHPGEOREF not specified.')
            config.gdf_georef = None

# unify the window background color
# https://wiki.wxpython.org/LongRunningTasks


#%% add matplotlib area

class NavigationToolbar(NavigationToolbar2Wx):
    # only display the buttons we need
    toolitems = [t for t in NavigationToolbar2Wx.toolitems if
                 t[0] not in ('Subplots', 'Home')]


def addaxes2frame(wxPnlPlot, addax=True):
    """
    this function adds the matplotlib figure axes canvas to a wxPanel
    """
    plt.style.use('ggplot')
    fig = Figure(dpi=100, facecolor=[0.9, 0.9, 0.9])
    canvas = FigureCanvas(wxPnlPlot, -1, fig)

    plotlbar = NavigationToolbar(canvas)
    plotlbar.Realize()
    plotlbar.update()

    sizer0 = wx.BoxSizer(wx.VERTICAL)
    sizer1 = wx.BoxSizer(wx.HORIZONTAL)
    sizer2 = wx.BoxSizer(wx.VERTICAL)

    sizer1.Add(canvas,   10, wx.EXPAND|wx.ALL, 1)
    sizer2.Add(plotlbar, 0, wx.EXPAND)

    sizer0.Add( sizer1, 1, wx.CENTER | wx.EXPAND )
    sizer0.Add( sizer2, 0, wx.CENTER )

    wxPnlPlot.SetSizer(sizer0)
    wxPnlPlot.Fit()

    fig.tight_layout(rect=(0.07, 0.06, 0.98, 1.0))
    if addax:
        ax = fig.add_subplot(111)
        return fig, ax, canvas
    else:
        return fig, canvas

#%%
def errMsg(txt):
    dlg = wx.MessageDialog(None, txt, 'Error', wx.OK | wx.ICON_ERROR)
    dlg.ShowModal()
    dlg.Destroy()
    return

def logLine(s):
    logging.info(s)

def stampPlot(ax):
    ax.annotate('Produced with NeDNR ' + APP_NAME + ' ver ' + VERSION,
                       xy=(0.01,0.99), xycoords='figure fraction', alpha=0.5,va='top',ha='left')


class runCmd_new( wx.Frame ) :
    """
    show stdout of a console to a textbox
    """
    def __init__( self, frm, wd, cmd):
        wx.Frame.__init__ ( self, frm,  title='Executing Scenario Run ... ')
        self.proc = None
        #self.Bind(wx.EVT_IDLE, self.OnIdle)

        self.txtMsg = wx.TextCtrl(self, wx.ID_ANY, size=(300, 400), style=wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
        self.txtMsg.SetFont(wx.Font(10, wx.MODERN, wx.NORMAL, wx.NORMAL, False, u'Consolas'))

        self.Show()
        self.runCmd(wd, cmd)

    def runCmd( self, wd, cmd ):
        os.chdir(wd)

        self.proc = wx.Process(self)
        self.proc.Redirect()
        self.pid = wx.Execute(cmd, wx.EXEC_ASYNC, self.proc)
        self.pin = self.proc.GetInputStream()
        while True:
            text = self.pin.readline()
            if (text is not None) and (text != ''):
                self.txtMsg.AppendText(text)
            if not self.proc.Exists(self.pid):
                return



#%%
def runCmd( frm, wd, cmd ):
    os.chdir(wd)

    frm.stdoutRef = sys.stdout
    frm.stderrRef = sys.stderr
    frmMsg = wx.Frame(frm,  title='Executing Scenario Run ... ')
    frmMsg.SetFont(wx.Font(10, wx.MODERN, wx.NORMAL, wx.NORMAL, False, u'Consolas'))
    txtMsg = wx.TextCtrl(frmMsg, wx.ID_ANY, size=(300, 400), style=wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
    txtLog = RedirectText(txtMsg)
    sys.stdout = txtLog
    sys.stderr = txtLog
    frmMsg.Show()
    print(cmd)
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc.poll()
#        proc = subprocess.run(exe.lower() + '.exe ' + self.nameFilePath, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    while True:
#            print(proc.stdout)
        line = proc.stdout.readline()
        wx.Yield()
        if line.strip() == "":
            pass
        else:
            print(line.decode().strip())
        if not line:
            break

    sys.stdout = frm.stdoutRef
    sys.stderr = frm.stderrRef
#        frmMsg.Close()


def changeZone(zoneType, wxChkZone, wxFilZone):
    if zoneType == 'customized':
        with wx.FileDialog(None, "Open zone file", style=wx.FD_OPEN  | wx.FD_FILE_MUST_EXIST) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_CANCEL:
                wxChkZone.SetSelection(0)
                f = 'zones\\' +  wxChkZone.GetItemLabel(0).lower() + '.zon'
            else:
                f =  fileDialog.GetPath()
    else:
        f = 'zones\\' +  zoneType + '.zon'
    
    if f.endswith('shp'):
        zfn = os.path.basename(f.strip()).rstrip('.shp')
        outputZoneFile = 'zones\\' + zfn + '.zon'
        os.chdir(config.name['MASTER'])
        ndim = [int(i) for i in config.name['NDIM'].split()]
        zon = Zone(f, ndim=ndim, gdf_grid=config.gdf_grid, rowfield='Row', 
            colfield='Column', zonfield='Name', outputZoneFile=outputZoneFile)
    else:    
        zon = Zone(f)

    if wxFilZone is not None:
        wxFilZone.SetPath(zon.zonefile)

    return zon
#%%
def plot_geo_ref(ax):
    if 'Polygon' in config.gdf_georef.geom_type.values:
        gax=config.gdf_georef.plot(ax=ax, facecolor='none', edgecolor='black', linewidth=0.5, alpha=config.labelAlpha)
        if config.name['SHPGEOREFLBL'] in config.gdf_georef.columns:
            for i, r in config.gdf_georef.iterrows():
                p = r.geometry.centroid
                #ax.scatter(p.x, p.y, marker='p', c='black')
                ax.annotate(r[config.name['SHPGEOREFLBL']], xy=(p.x, p.y),
                            ha='center', va='center', alpha=config.labelAlpha)
    else:
        gax=config.gdf_georef.plot(ax=ax, color='grey', alpha=config.labelAlpha)
        if config.name['SHPGEOREFLBL'] in config.gdf_georef.columns:
            for i, r in config.gdf_georef.iterrows():
                p = r.geometry.centroid
                #ax.scatter(p.x, p.y, marker='p', c='black')
                ax.annotate(r[config.name['SHPGEOREFLBL']], xy=(p.x, p.y),
                            xytext=(0, 15), textcoords='offset points',
                            ha='center', va='bottom', alpha=config.labelAlpha)
    return gax

def plot_arr_map(arr, mask, cmap=None, shrink=0.8, legend=False, legend_title='',
                 ax=None, vmin=None, vmax=None, extend='neither', **kwargs):
    arr = np.where(mask!=0, arr, np.nan)

    if ax is None:
        fig, ax = plt.subplots()
    aimg=ax.imshow(arr, cmap=cmap, vmin=vmin, vmax=vmax, **kwargs)
    ax.axis('off')
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    if legend:
        ax.figure.colorbar(aimg, shrink=shrink, label=legend_title, extend=extend)
    return arr, aimg


def shiftedColorMap(cmap, start=0, midpoint=0.5, stop=1.0, name='shiftedcmap'):
    '''
    Function to offset the "center" of a colormap. Useful for
    data with a negative min and positive max and you want the
    middle of the colormap's dynamic range to be at zero

    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower ofset). Should be between
          0.0 and `midpoint`.
      midpoint : The new center of the colormap. Defaults to
          0.5 (no shift). Should be between 0.0 and 1.0. In
          general, this should be  1 - vmax/(vmax + abs(vmin))
          For example if your data range from -15.0 to +5.0 and
          you want the center of the colormap at 0.0, `midpoint`
          should be set to  1 - 5/(5 + 15)) or 0.75
      stop : Offset from highets point in the colormap's range.
          Defaults to 1.0 (no upper ofset). Should be between
          `midpoint` and 1.0.
    '''
    from matplotlib.colors import LinearSegmentedColormap
    import matplotlib.pyplot as plt
    cdict = {'red': [], 'green': [], 'blue': [], 'alpha': []}

    # regular index to compute the colors
    reg_index = np.linspace(start, stop, 257)

    # shifted index to match the data
    shift_index = np.hstack([
        np.linspace(
            0.0, midpoint, 128, endpoint=False), np.linspace(
                midpoint, 1.0, 129, endpoint=True)
    ])

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))

    newcmap = LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap

#%%
class RedirectText(object):
    def __init__(self,aWxTextCtrl):
        self.out=aWxTextCtrl

    def write(self,string):
        self.out.WriteText(string)



###########################################################################
## Class FrameZMAP
###########################################################################

class FrameZMAP( wx.Frame ) :
    """
    plot the zone distribution
    """
    def __init__( self, parent, zone, w):

        h = 650 / (config.ndim[1] * 1.2) * config.ndim[2] + 20
        wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = APP_NAME + ': Map of Zone Distribution',
                           pos = wx.DefaultPosition, size = wx.Size( h , w ),
                           style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
        self.SetBackgroundColour( config.winBgColor )
        self.SetIcon(config.appIcon)

        #self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

        bSizer46 = wx.BoxSizer( wx.VERTICAL )

        self.wxChkGeoRef = wx.CheckBox( self, wx.ID_ANY, u"Show Spatial Reference", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer46.Add( self.wxChkGeoRef, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )
        self.wxChkGeoRef.Bind( wx.EVT_CHECKBOX, self.showGeoRef )


        self.wxPnlPlot = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer46.Add( self.wxPnlPlot, 1, wx.EXPAND |wx.ALL, 5 )

        self.SetSizer( bSizer46 )
        self.Layout()

        self.Centre( wx.BOTH )

        self.figure, self.canvas = addaxes2frame(self.wxPnlPlot, False)
        self.zone = zone
        self.zone.zarray = zone.zarray * config.mask
        # make plot
        self.showGeoRef(None)


    def showGeoRef(self, event):
        self.figure.clear()
        self.axes = self.figure.add_subplot(111)

        self.zone.plot(self.axes)
        if self.wxChkGeoRef.GetValue():
            plot_geo_ref(self.axes)

        stampPlot(self.axes)
        self.canvas.draw()






class FrameLLUStat ( wx.Frame ):
    """
    show land use statistics
    """
    def __init__( self, parent ):
        wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = APP_NAME + ": Baseline Land Use: Zonal Statistics", pos = wx.DefaultPosition, size = wx.Size( 528,472 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

        self.SetBackgroundColour( config.winBgColor )
        self.SetIcon(config.appIcon)
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

        bSizer46 = wx.BoxSizer( wx.VERTICAL )

        self.wxPnlPlot = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer46.Add( self.wxPnlPlot, 1, wx.EXPAND |wx.ALL, 5 )

        bSizer47 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_button10 = wx.Button( self, wx.ID_ANY, u"Export Data", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer47.Add( self.m_button10, 0, wx.ALL, 5 )

        self.wxSldYear = wx.Slider( self, wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_HORIZONTAL|wx.SL_LABELS )
        bSizer47.Add( self.wxSldYear, 1, wx.EXPAND|wx.ALIGN_BOTTOM, 0 )


        bSizer46.Add( bSizer47, 0, wx.EXPAND, 0 )


        self.SetSizer( bSizer46 )
        self.Layout()

        self.Centre( wx.BOTH )

        # Connect Events
        self.m_button10.Bind( wx.EVT_BUTTON, self.exportData )
        self.wxSldYear.Bind( wx.EVT_SCROLL_CHANGED, self.changeYear )

        self.initPlot(config.name, config.ndim, config.zname, config.zarray)


    def __del__( self ):
        pass

    def initPlot(self, name, ndim, zname, zarray):

        # set up plot and add axes
        self.figure, self.axes, self.canvas = addaxes2frame(self.wxPnlPlot)

        #print(name)
        self.name = name
        self.ndim, self.zname, self.zarray = ndim, zname, zarray
        yr1 = int(name['YEARS'].split()[0])
        yr2 = int(name['YEARS'].split()[1])
        self.wxSldYear.SetMin(yr1)
        self.wxSldYear.SetMax(yr2)
        self.wxSldYear.SetValue(yr2)

    # Virtual event handlers, overide them in your derived class
    def exportData( self, event ):
        event.Skip()

    def changeYear( self, event ):
        yr = self.wxSldYear.GetValue()
        zarray = self.zarray.flatten()
        os.chdir(os.path.join(self.name['MASTER'], self.name['LUDIR']))
        prefix = self.name['PREFIXLU']
        ori = pd.read_csv(prefix + str(yr) + '.txt', skipinitialspace=True, header=0)

        zones = [self.zname[zarray[c-1]-1] for c in ori.iloc[:, 0]]
        stat = ori.iloc[:,2:].groupby(zones).sum()
        stat = stat.where(stat != 0)
        self.axes.clear()
        stat.plot.bar(ax = self.axes)

        stampPlot(self.axes)
        self.canvas.draw()

class frameConfig(frmConfig):
    def __init__(self, parent, show=False):
        super().__init__(parent)
        try:

            self.SetBackgroundColour( config.winBgColor )
            self.SetIcon(config.appIcon)

            with open(os.path.join(config.appDir, 'config.ini')) as ini: sini = ini.readlines()
            self.wxFilName.SetPath(sini[0].strip())
            self.wxDirMaster.SetPath(sini[1].strip())
            self.changeNameFile(self.wxFilName)
        except OSError as e:
            logging.exception('Configuration error:\n' + str(e))
        finally:
            if show:
                self.Show()
        #errMsg(self.appdir)

    def confirmConfig(self, event):
        try:
            p = self.wxFilName.GetPath()
            d = self.wxDirMaster.GetPath()
            #config.workdir = os.path.basename(p)
            if not os.path.exists(p):
                errMsg('Can not find the name file: ' + p)
                return
            if not os.path.exists(d):
                errMsg('Can not find the directory: ' + d)
                return

            config.loadNamefile(p)
            config.outdir = d
            self.Close()

        except OSError as e:
            logging.exception('Configuration error:\n' + str(e))


    def gotoHelp(self, event):
        webbrowser.open(r"https://dui-gui-sustain.readthedocs.io/en/latest/windows/main.html")

    def quitMain(self, event):
        with open(os.path.join(config.appDir, 'config.ini'), 'w') as ini:
            ini.write(self.wxFilName.GetPath() + '\n')
            ini.write(self.wxDirMaster.GetPath() + '\n')
        self.Destroy()


    def changeNameFile( self, event ):
        pass


    def changeOutDir( self, event ):
        pass



class frameScnCreate(frmScnCreate):
    """
    make changes to pumping and recharge
    """

    plotType = 'line'
    nChange = 0
    nameFilePath = ''
    init0 = True
    loaded = False
    plotVars0 = ('Precipitation', 'Groundwater pumping', 'Surface Delivery',
                'Streamflow')
    plotVars1 = ('Direct DP','Canal Seepage', 'Reinfiltration')
    plotVars2 = ('Direct ET', 'ETtrans', 'Surface Loss', 'Transmission Loss')
    # a surface loss was calculated to determine the portion of applied water that was lost directly to non-beneficial consumptive use
    # TODO: pump layer


    def __init__( self, parent):
        super().__init__(parent)
        self.SetBackgroundColour( config.winBgColor )
        self.SetTitle(APP_NAME + ': Create New Pumping/Recharge Scenario')
        self.SetIcon(config.appIcon)
        self.saved = False

        self.name = config.name.copy()


        os.chdir(self.name['MASTER'])
        self.name['OUTDIR'] = config.outdir.rstrip('\\').rstrip('/')

        #self.wxFilZone.SetPath('zones/county.zon')


        self.startYear = []
        self.endYear = []

        self.pmpRate = []
        self.Months = []

        self.Zones = []
        self.Relative = []

        self.init0 = False

        self.changePeriod(None)

        self.wxLstChange.AppendColumn('Zone', wx.LIST_FORMAT_RIGHT, width=180)
        self.wxLstChange.AppendColumn('Start Year', wx.LIST_FORMAT_CENTER, width=70)
        self.wxLstChange.AppendColumn('End Year', wx.LIST_FORMAT_CENTER, width=70)
        self.wxLstChange.AppendColumn('Month', wx.LIST_FORMAT_CENTER, width=250)
        self.wxLstChange.AppendColumn('Type', wx.LIST_FORMAT_CENTER, width=60)
        self.wxLstChange.AppendColumn('Rate', wx.LIST_FORMAT_CENTER, width=50)


        # initial input zones
        self.zone = Zone(os.path.join(self.name['MASTER'], 'zones/county.zon'), self.name)
        self.wxClbFromZone.InsertItems(list(self.zone.znames), 0)
        self.wxCheckPercent.SetValue(True)
        self.enableTxtPumpRate()

    def changePeriod(self, event):
        iPeriod = -1 #self.wxChkPeriod.GetSelection()
        self.mf = Modflow(self.name['MFNAM'].split()[iPeriod], self.name, historical=False)

        # assign time for time steps and stress period

        ymin = self.mf.dis_time['Date'][0].year
        ymax = self.mf.dis_time['Date'][-1].year

        self.wxSpinStartYear.SetMin(ymin)
        self.wxSpinStartYear.SetMax(ymax)
        self.wxSpinEndYear.SetMin(ymin)
        self.wxSpinEndYear.SetMax(ymax)
        self.wxSpinStartYear.SetValue(ymin)
        self.wxSpinEndYear.SetValue(ymax)


        #TODO: pumping layers
        self.play = np.ones((self.mf.nrow, self.mf.ncol)).astype(int)

        # read well files
        if not self.mf.mf_read_well():
            return


    def enableTxtPumpRate(self):
        self.wxTxtWellPercent.Enable(self.wxCheckPercent.Value)
        self.wxTxtWellAf.Enable(self.wxCheckAf.Value)
        self.wxTxtNewWel.Enable(self.wxCheckNew.Value)
        self.wxTxtNewRch.Enable(self.wxCheckNewRch.Value)
        self.wxTxtNewWel.SetValue('')
        self.wxTxtWellAf.SetValue('')
        self.wxTxtWellPercent.SetValue('')


    def checkPercent( self, event ):
        self.enableTxtPumpRate()

    def checkAf( self, event ):
        self.enableTxtPumpRate()

    def checkNew( self, event ):
        self.enableTxtPumpRate()

    def addChange( self, event ):

        izones = np.array(self.wxClbFromZone.GetCheckedItems())
        if len(izones) == 0:
            izones = np.arange(self.zone.nzone)
            self.zoneTxt = 'All Zones'
        else:
            self.zoneTxt = ','.join( self.wxClbFromZone.GetCheckedStrings())

        imonths = np.array(self.wxClbMonth.GetCheckedItems())
        if len(imonths) == 0:
            imonths = np.arange(12)
            self.monTxt = 'All Months'
        else:
            self.monTxt = ','.join([str(s)[:3] for s in self.wxClbMonth.GetCheckedStrings()])


        sdate = self.wxSpinStartYear.GetValue()
        edate = self.wxSpinEndYear.GetValue()

        if edate < sdate:
            errMsg('Start year must smaller than To year')
            return False

        rel = (self.wxCheckPercent.Value, self.wxCheckAf.Value, self.wxCheckNew.Value or self.wxCheckNewRch.Value)


        # calculate the rate for recharge for the second and third situation
        izones = izones + 1
        crate = 0
        if rel[0]:
            prate = float(self.wxTxtWellPercent.Value or 0)
            crate = prate / 100.0
            ptype = '%'
        if rel[1]:
            crate = prate * ACRE2FOOT / DAYS_IN_MONTH[imonths].sum()
            ptype = 'vol'
        if rel[2]:
            prate = float(self.wxTxtNewRch.Value or 0) - float(self.wxTxtNewWel.Value or 0)
            nZoneCell = np.count_nonzero([z in izones for z in self.zone.zarray.flatten() * config.mask.flatten()])
            crate = prate  * ACRE2FOOT / nZoneCell / DAYS_IN_MONTH[imonths].sum()
            ptype = 'new vol'

        if crate == 0:
            errMsg('Please specify the changes in the selected text box.')
            return False
        

        self.wxLstChange.Append([self.zoneTxt, str(sdate), str(edate), self.monTxt, ptype, str(prate)])
        
        self.startYear.append(sdate)
        self.endYear.append(edate)
        self.Zones.append(izones)
        self.Months.append(imonths)
        self.pmpRate.append(crate)
        self.Relative.append(rel)

        self.nChange += 1


    def showZone( self, event ):
        # Connect Events
        w = 650
        wxfrmStat = FrameZMAP(self, self.zone, w)
        wxfrmStat.Show()


    def resetChange(self, event):
        self.saved = False
        for i in range(self.nChange):
            self.removeChange(0)
        self.nChange = 0

    def changeZone(self, event):
        self.zone = changeZone(self.wxChkZone.GetStringSelection().lower(), self.wxChkZone, None)

        self.wxClbFromZone.Clear()
        self.wxClbFromZone.InsertItems(self.zone.znames, 0)
        self.resetChange(None)

    def removeChange(self, i):
        self.wxLstChange.DeleteItem(i)
        self.startYear.pop(i)
        self.endYear.pop(i)
        self.Zones.pop(i)
        self.Months.pop(i)
        self.pmpRate.pop(i)
        self.Relative.pop(i)
        self.nChange -= 1

    def deleteChange( self, event ):
        while self.wxLstChange.GetFirstSelected() > -1:
            self.removeChange(self.wxLstChange.GetFirstSelected())


    def runModel( self, event ):
        if not self.saved:
            errMsg('Please save changes before execute the model')
            return
        cmd = os.path.join(self.name['MASTER'], 'run_mf.bat') + ' ' + self.name['OUTDIR']
        runCmd(self, self.name['MASTER'], cmd)

    def saveFile( self, event ):

        frm = frameProgress('Saving Changes', 'Progress', int(self.mf.nper*2), parent=self)
        if self.nChange <= 0:
            errMsg('No change to be saved')
            return

        os.chdir(self.name['MASTER'])


        pmps = []
        maxwel = 0

        slog = 'Number of change: ' + str(self.nChange) + '\n' + 'Output zone file: "' + self.zone.zonefile + '"\n'

        for j in range(self.nChange):
            i = j + 1
            slog += '-------------------------------------------------------------\n'
            unit = '(%)      ' if self.Relative[j][0] else '(ft3/day)'
            slog += 'Input Zone ' + str(i) + ' : ' + str(self.Zones[j]) + '\n' + \
                    'Start Year ' + str(i) + ' : ' + str(self.startYear[j]) + '\n' + \
                    'End Year ' + str(i) + '   : ' + str(self.endYear[j]) + '\n' + \
                    'Months ' + str(i) + '     : ' + self.monTxt + '\n' + \
                    'Relative ' + str(i) + '   : ' + str(self.Relative[j])  + '\n' + \
                    'Change Rate ' + unit + str(i) + ': ' + str(self.pmpRate[j]) + '\n'

        logging.info(slog)

        for iper, p in zip(range(self.mf.nper), self.mf.welData):
            frm.setValue(iper + 1, 'Applying changes to Well file:')
            date = self.mf.dis_time.loc[(iper+1,1),'Date']
            mon = date.month - 1
            #print(p)
            #p = pd.read_fwf(StringIO('\n'.join(pmp)), widths=[10,10,10,10], header=None, names=['layer','row','col','rate'])
            p.loc[:, 'zone'] = self.zone.zarray[p['row'] - 1, p['col'] -1 ]

            for s, e, zs, ms, r, rel in zip(self.startYear, self.endYear, self.Zones, self.Months, self.pmpRate, self.Relative):
                if mon in ms and date.year >= s and date.year <= e:
                    if rel[0]:
                        iwells = [iz in zs for iz in p['zone']]
                        p.loc[iwells, 'rate'] = p.loc[iwells, 'rate'] * r
                    if rel[1]:
                        iwells = [iz in zs for iz in p['zone']]
                        p.loc[iwells, 'rate'] = p.loc[iwells, 'rate'] + r

                    if rel[2]:
                        icell = 0
                        rows = []
                        cols = []
                        lays = []

                        for ir in range(self.mf.nrow):
                            for ic in range(self.mf.ncol):
                                iz = self.zone.zarray[ir, ic]
                                if iz in zs:
                                    rows.append(ir + 1)
                                    cols.append(ic + 1)
                                    lays.append(self.play[ir, ic])
                                icell += 1
                        p = p.append(pd.DataFrame({'row': rows,
                                               'col': cols,
                                               'layer': lays,
                                               'rate': r}))

            pmps.append(p.loc[:, ['layer','row','col','rate']])
            maxwel = max(maxwel, p.shape[0])

        # save well file
        welfile = os.path.join(self.name['OUTDIR'], 'scen.wel')
        self.mf.mf_write_well(welfile,  maxwel, self.mf.IWELCB, pmps, frm)

        # save modflow name file
        newMfName = self.mf.name.copy()
        self.mf.mf_write_name(os.path.join(self.name['OUTDIR'], 'baseline.nam'), newMfName)
        newMfName.loc['WEL', 'file'] = os.path.join(self.name['OUTDIR'], 'scen.wel')
        newMfName.loc['LIST', 'file'] = os.path.join(self.name['OUTDIR'], 'scen.lst')
        newMfName.loc['HDS', 'file'] = os.path.join(self.name['OUTDIR'], 'scen.hds')
        newMfName.loc['CBC', 'file'] = os.path.join(self.name['OUTDIR'], 'scen.cbb')
        if 'DDN' in newMfName.index:
            newMfName.loc['DDN', 'file'] = os.path.join(self.name['OUTDIR'], 'scen.ddn')
        self.mf.mf_write_name(os.path.join(self.name['OUTDIR'], 'scen.nam'), newMfName)

        # save time
        self.mf.dis_time.to_csv(os.path.join(self.name['OUTDIR'], 'timetable.csv'))

        self.saved = True
        frm.Destroy()
        dlg = wx.MessageDialog(self, 'Changes have been successfully saved. \n Execute model now?', 'Complete Saving', wx.YES_NO | wx.NO_DEFAULT | wx.ICON_INFORMATION)
        modal = dlg.ShowModal()
        dlg.Destroy()
        if (modal == wx.ID_YES):
            self.runModel(None)


    def showHelp( self, event ):
        dlg = wx.MessageDialog(self, '`Change existing wells (%)` will multiply the pumping rates of the existing wells\n' + \
                                     '  in the selected zones with the percentage provided in the textbox next to it. \n\n' + \
                                     '`Change existing wells (af/yr)` will add the percentage provided in the textbox\n' + \
                                     '  the pumping rates of the existing wells in the selected zones. \n\n' + \
                                     '`Add new wells (af/yr)` will add new wells to each individual grid cell in the selected zones.\n' + \
                                     '  The change value for this option can be, for example,\n' + \
                                     '  `+100` meaning to add 100 acre-feet per year and distributed to the cells in the selected zones.\n' + \
                                     '  `-100` meaning to pump wells 100 acre-feet per year and distributed to the cells in the selected zones.'
                , 'Help', wx.OK | wx.ICON_INFORMATION)
        dlg.ShowModal()


class frameWatshedBud(frmWatshedBud):
    init0 = True
    plotVars0 = ('Groundwater pumping', 'Recharge', 'Acres')
    #plotVars1 = ('Direct DP','Canal Seepage', 'Reinfiltration')
    #plotVars2 = ('Direct ET', 'ETtrans', 'Surface Loss', 'Transmission Loss')

    def __init__(self, parent, historical=True):
        super().__init__(parent)
        self.loaded = False

        # appearence
        self.SetBackgroundColour( config.winBgColor )
        self.SetIcon(config.appIcon)

        title = 'Historical' if historical else 'Future'
        self.SetTitle(APP_NAME + ': ' + title + ' Watershed Model Data')

        self.wxTrcPlotVar.DeleteAllItems()
        plotVarRoot = self.wxTrcPlotVar.AddRoot('Variable Name')

        for vn in self.plotVars0:
            self.wxTrcPlotVar.AppendItem(plotVarRoot, vn)

        # pass the data
        # create canvas and add axes
        self.figure, self.axes, self.canvas = addaxes2frame(self.wxPnlPlot)
        # create the zones
        self.changeZone(None)
        self.rswb = Rswb(namefile=config.name, historical=historical)



    def loadResults( self, event ):
        """
        read the REPORT results
        get crop areas by zones
        """

        # load zone
        # progress bar
        frmProgress = frameProgress('Loading results from "' + self.rswb['OUTDIR'].strip() + '"', 'Progress:', self.rswb.years.shape[0])
        self.rswb.zonbud(self.zone, frmProgress, self.rswb['IRRI'].split(), self.rswb['CROP'].split())

        self.dfBase = self.rswb.df
        self.wxClbPlotZone.Clear()
        self.wxClbPlotZone.InsertItems(self.dfBase['Zone'].unique(), 0)

        self.wxClbPlotCrop.Clear()
        self.wxClbPlotCrop.InsertItems(self.dfBase['Crop'].unique(), 0)

        self.wxClbPlotIrr.Clear()
        self.wxClbPlotIrr.InsertItems(self.dfBase['Irr_Type'].unique(), 0)

        self.axes.clear()
        self.loaded = True

    def changeZone(self, event):
        self.zone = changeZone(self.wxChkZone.GetStringSelection().lower(),
                               self.wxChkZone, None)


    def showZone( self, event ):
        # Connect Events

        w = 650
        wxfrmStat = FrameZMAP(self, self.zone, w)
        wxfrmStat.Show()


    def makePlot( self, event ):

        self.axes.clear()

        selectedFlow = []

        for itm in self.wxTrcPlotVar.GetSelections():
            selected = self.wxTrcPlotVar.GetItemText(itm)
            selectedFlow.append(selected)
        selectedFlow = list(set(selectedFlow))
        if len(selectedFlow) == 0:
            dlg = wx.MessageDialog(self, 'Must select a variable before plotting', 'Error', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
            return

        self.dfPlot = self.rswb.zonplot(ax=self.axes,
                          selectedFlow=selectedFlow,
                          selectedIrr=self.wxClbPlotIrr.GetCheckedStrings(),
                          selectedCrop=self.wxClbPlotCrop.GetCheckedStrings(),
                          selectedZone=self.wxClbPlotZone.GetCheckedStrings(),
                          AggCrop=self.wxChkAggCrop.GetValue(),
                          AggIrr=self.wxChkAggIrr.GetValue(),
                          AggZone=self.wxChkAggZone.GetValue(),
                          CumSum=self.wxChkCum.GetValue(),
                          plotType=self.wxChiPlotType.GetString(self.wxChiPlotType.GetSelection()).lower(),
                          stacked=self.wxChkStacked.GetValue(),
                          xlbl=self.wxTxtXLabel.GetValue().strip(),
                          ylbl=self.wxTxtYLabel.GetValue().strip(),
                          tle=self.wxTxtPlotTitle.GetValue().strip())

        stampPlot(self.axes)
        self.canvas.draw()

    def exportData( self, event ):
        with wx.FileDialog(self, "Export plot data", wildcard="CSV files (*.csv)|*.csv", style=wx.FD_SAVE) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind
            # Proceed loading the file chosen by the user
            self.dfPlot.to_csv(fileDialog.GetPath(), index=False)


class frameModflowBud(frmModflowBud):
    loaded = False



    def __init__( self, parent, datasets=['Baseline'], historical=True):
        super().__init__(parent)

        self.SetBackgroundColour( config.winBgColor )
        self.SetIcon(config.appIcon)

        # set up plot and add axes
        self.figure, self.axes, self.canvas = addaxes2frame(self.wxPnlPlot)

        # set window title
        title = 'Historical' if historical else 'Future'
        self.SetTitle(APP_NAME + ': ' + title + ' Groundwater Model Budget')

        # set selectable variables
        self.wxClbPlotVar.InsertItems(['Groundwater Pumping', 'Baseflow (Stream)', 'Storage Change'], 0)
        # set seletable model
        self.wxChiDfType.Clear()
        self.wxChiDfType.AppendItems(datasets)
        self.wxChiDfType.Select(0)



        # baseline dis
        self.name = config.name.copy()

        iMod = 0 if historical else 1

        self.mf_bas = Modflow(self.name['MFNAM'].split()[iMod], self.name, historical)
        if ('Scenario' in datasets) or ('Difference' in datasets) or ('Both' in datasets):
            self.mf_scn = Modflow(os.path.join(config.outdir, 'scen.nam'), self.name, historical)
        else:
            self.mf_scn = None

        # assume it is county zone when the window is activated
        self.changeZone(None)



    def showZone( self, event):
        # Connect Events
        w = 650
        wxfrmStat = FrameZMAP(self, self.zone, w)
        wxfrmStat.Show()


    def changeZone( self, event ):
        self.zone = changeZone(self.wxChkZone.GetStringSelection().lower(),
            self.wxChkZone, None)
        self.wxClbPlotZone.Clear()




    def loadResults( self, event ):

        # run zonbud flopy
        os.chdir(self.name['MASTER'])
        # calculate the zone budget using flopy
        try:
            frm = frameBusy('Loading MODFLOW Budget ... ')
    
            self.dfBase = self.mf_bas.zonbud(self.zone)
            if self.mf_scn is not None:
                self.dfScen = self.mf_scn.zonbud(self.zone)
                # estimate the difference
                self.dfDiff = self.dfScen - self.dfBase

        except Exception as e:
            logging.exception(repr(e))
            errMsg(repr(e))
            return

        self.wxClbPlotZone.InsertItems(self.dfBase.columns.tolist(), 0)
        self.loaded = True




    def makePlot( self, event ):
        if not self.loaded:
            errMsg('Must read results before plotting')
            return

        self.axes.clear()


        if self.wxChiDfType.GetString(self.wxChiDfType.GetSelection()).lower() == 'baseline':
            df = self.dfBase; df['Model'] = 'Baseline'
        elif self.wxChiDfType.GetString(self.wxChiDfType.GetSelection()).lower() == 'scenario':
            df = self.dfScen; df['Model'] = 'Scenario'
        elif self.wxChiDfType.GetString(self.wxChiDfType.GetSelection()).lower() == 'difference':
            df =  self.dfDiff
            df['Model'] = 'Difference'
        elif self.wxChiDfType.GetString(self.wxChiDfType.GetSelection()).lower() == 'both':
            df1 = self.dfBase; df1['Model'] = 'Baseline'
            df2 = self.dfScen; df2['Model'] = 'Scenario'
            df = pd.concat([df1, df2], axis=0)

        # aggregate zones
        self.dfPlot = self.mf_bas.zonplot(df, ax=self.axes,
                                          selectedFlow=list(self.wxClbPlotVar.GetCheckedStrings()),
                                          selectedZone=list(self.wxClbPlotZone.GetCheckedStrings()),
                                          AggVar=self.wxChkAggVar.GetValue(), AggZone=self.wxChkAggZone.GetValue(),
                                          plotType=self.wxChiPlotType.GetString(self.wxChiPlotType.GetSelection()).lower(),
                                          month=self.wxChkMonth.GetValue(), stacked=self.wxChkStacked.GetValue(),
                                          CumSum=self.wxChkCum.GetValue(),
                                          xlbl=self.wxTxtXLabel.GetValue().strip(),
                                          ylbl=self.wxTxtYLabel.GetValue().strip(),
                                          tle=self.wxTxtPlotTitle.GetValue().strip()
                                          )



        stampPlot(self.axes)
        self.canvas.draw()

    def exportData( self, event ):
        with wx.FileDialog(self, "Export plot data", wildcard="CSV files (*.csv)|*.csv", style=wx.FD_SAVE) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind
            # Proceed loading the file chosen by the user
            self.dfPlot.to_csv(fileDialog.GetPath())


class frameGwLevel(frmGwLevel):
    loaded = False
    lbl = ''
    updateValue = True
    head = None

    def __init__( self, parent, datasets=['Baseline'], historical=True):
        super().__init__(parent)
        self.SetBackgroundColour( config.winBgColor )
        self.SetTitle(APP_NAME + ': Groundwater Level')
        self.SetIcon(config.appIcon)

        # set up plot
        self.figure, self.canvas = addaxes2frame(self.wxPnlPlot, False)
        #self.axes.set_title('Welcome to MF plot!')

        # selectable variable to plot
        self.wxChiPlotType.Clear()
        self.wxChiPlotType.Append('Difference')
        self.wxChiPlotType.Select(0)

        self.wxChkGeoRef.SetLabel('Show Spatial Reference')
        # other variable
        self.name = config.name


        iMod = 0 if historical else 1
        self.mf_bas = Modflow(self.name['MFNAM'].split()[iMod], self.name, historical)
        if ('Scenario' in datasets) or ('Difference' in datasets) or ('Both' in datasets):
            self.mf_scn = Modflow(os.path.join(config.outdir, 'scen.nam'), self.name, historical)
        else:
            self.mf_scn = None



        self.loadResults()


    def loadResults( self ):

        # get the head files
        os.chdir(self.name['MASTER'])
        headbas_f = self.mf_bas.name.loc['HDS','file']
        self.headbas   = HeadFile(headbas_f)

        if self.mf_scn is not None:
            os.chdir(self.name['MASTER'])
            headscn_f = self.mf_scn.name.loc['HDS','file']
            self.headscn   = HeadFile(headscn_f)
        else:
            self.headscn = None

        # load time
        self.wxChiPlotYear.Clear()
        self.wxChiPlotMonth.Clear()

        dis_time = pd.DatetimeIndex(self.mf_bas.dis_time['Date'])

        self.wxChiPlotYear.AppendItems([str(dt) for dt in dis_time.year.unique()])
        self.wxChiPlotMonth.AppendItems([str(dt) for dt in dis_time.month.unique()])
        self.wxChiPlotYear.Select(0)
        self.wxChiPlotMonth.Select(0)

        self.loaded = True

        self.wxSpinLayer.SetMax(config.ndim[0])

    def retriveHeads(self, idx, ilayer, baseline=True):
        # read data
        h0 = self.headbas.get_data(idx=idx) if baseline else self.headscn.get_data(idx=idx)
        hh = h0[ilayer,:,:] * config.mask
        # filter dry cells
        hdry  = float(config.name['MFHDRY'])
        dhdry = np.abs(hdry) * 0.01
        hmin = hdry-dhdry
        hmax = hdry+dhdry
        return(np.where((hh>hmin) & (hh<hmax), np.nan, hh))


    def showGeoRef(self, event):
        self.makePlot(None)

    def makePlot( self, event ):
        if not self.loaded:
            errMsg('Must read results before plotting')
            return
        self.figure.clear()
        self.axes = self.figure.add_subplot(111)

        year = self.wxChiPlotYear.GetStringSelection()
        month = self.wxChiPlotMonth.GetStringSelection()


        if year == wx.NOT_FOUND or month == wx.NOT_FOUND:
            errMsg('Must select the time for plotting')
            return

        #plot_arr_map(1, self.mask, ax=self.axes, cmap='Greys_r', vmin=-2.5, vmax=6)

        itime = np.abs(self.mf_bas.dis_time['Date'] - pd.Timestamp(year + '-' + month + '-31')).values.argmin()
        if self.updateValue:
            ptype = self.wxChiPlotType.GetString(self.wxChiPlotType.GetSelection())
            ilayer = self.wxSpinLayer.GetValue()-1
            if ptype == 'Baseline':
                self.head = self.retriveHeads(itime, ilayer)
                self.lbl = 'Baseline Groundwater Level (ft)'
            elif ptype == 'Scenario':
                self.head = self.retriveHeads(itime, ilayer, False)
                self.lbl = 'Scenario Groundwater Level (ft)'
            else:
                h0 = self.retriveHeads(itime, ilayer)
                h1 = self.retriveHeads(itime, ilayer, False)
                self.head = h1 - h0
                self.lbl = 'Groundwater Level Change (ft)'

            vmin = np.nanmin(self.head)
            vmax = np.nanmax(self.head)
            if vmin*vmax<0:
                vmid = 0
            else:
                vmid = (vmin + vmax) * 0.5

            self.wxTxtVmin.SetValue(str(vmin))
            self.wxTxtVmax.SetValue(str(vmax))
            self.wxTxtMidValue.SetValue(str(vmid))
            self.wxTxtRange.SetValue('The data range is: ({0:.2f}, {1:.2f})'.format(vmin, vmax) )

            self.updateValue = False
        else:
            vmin = float(self.wxTxtVmin.GetValue())
            vmax = float(self.wxTxtVmax.GetValue())
            vmid = float(self.wxTxtMidValue.GetValue())
            if vmid < vmin or vmid > vmax:
                vmid = (vmin + vmax) * 0.5

        # check if vmin and vmax make sense
        if vmin >= vmax:
            vmin=None;vmax=None;midpoint=0.5
            self.wxTxtRange.SetValue('The data uniformly equate to ' + str(vmid))
        else:
            midpoint = (vmid - vmin)/(vmax-vmin)
        cmap = shiftedColorMap(plt.get_cmap(self.wxChiPlotColor.GetStringSelection()), midpoint=midpoint)
        #cmap.set_under('black')
        aimg=self.axes.pcolormesh(self.head * config.mask, cmap=cmap, vmin=vmin, vmax=vmax)

        if self.wxChkGeoRef.GetValue():
            plot_geo_ref(self.axes)

        self.axes.axis('off')
        self.axes.xaxis.set_visible(False)
        self.axes.yaxis.set_visible(False)

        self.axes.set_xlim(0, config.ndim[2])
        self.axes.set_ylim(config.ndim[1], 0)

        self.figure.colorbar(aimg, shrink=0.8, label=self.lbl)


        #self.dfResults.plot(kind=self.plotType)
        xlbl = self.wxTxtXLabel.GetValue().strip()
        ylbl = self.wxTxtYLabel.GetValue().strip()
        tle = self.wxTxtPlotTitle.GetValue().strip()

        if xlbl != '':
            self.axes.set_xlabel(xlbl)

        if ylbl != '':
            self.axes.set_ylabel(ylbl)


        if tle != '':
            self.axes.set_title(tle)

        stampPlot(self.axes)
        self.canvas.draw()

    def exportShp( self, event ):
        if self.head is None:
            errMsg('Make plot before save data')
            return

        with wx.FileDialog(self, "Export as shapefiles", wildcard="Shapefiles (*.shp)|*.shp", style=wx.FD_SAVE) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind
            # Proceed loading the file chosen by the user

            os.chdir(config.name['MASTER'])
            gdf_grid = gpd.read_file(config.name['SHPGRID'].strip())

            gdf_grid['GWLvl'] = self.head[gdf_grid[config.name['SHPGRIDROW']] - 1, gdf_grid[config.name['SHPGRIDCOL']] - 1]
            gdf_grid.to_file(fileDialog.GetPath())


    def exportText( self, event ):
        if self.head is None:
            errMsg('Make plot before save data')
            return

        with wx.FileDialog(self, "Export as text", wildcard="Text file (*.txt)|*.txt", style=wx.FD_SAVE) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind
            # Proceed loading the file chosen by the user
            np.savetxt(fileDialog.GetPath(),self.head, fmt='%s')

    def plotTimeChange(self, event):
        self.updateValue = True

    def plotTypeChange(self, event):
        self.updateValue = True

    def plotLayerChange(self, event):
        self.updateValue = True


class frameMain(frmMain):

    def __init__(self, parent):
        super().__init__(parent)
        try:
            config.appIcon = wx.Icon('DNR.ico', wx.BITMAP_TYPE_ANY)
            config.winBgColor = wx.SystemSettings.GetColour(wx.SYS_COLOUR_MENU)
            config.appDir = os.getcwd()

            self.SetTitle('NeDNR ' + APP_NAME + ' ver ' + VERSION)
            self.SetBackgroundColour( config.winBgColor )
            self.SetIcon(config.appIcon)


        except:
            logging.exception('Error at Main Window')
        else:
            return



    ###########################################################################
    ###########################################################################

    def showConfig(self, event):
        frm = frameConfig(self, True)
        frm.ShowWindowModal()
        frm.Raise()

    def showHisWatershedBud(self, event):
        frm = frameWatshedBud(self, True)
        frm.Show()
        frm.Raise()

    def showHisModBud(self, event):
        frm = frameModflowBud(self)
        frm.Show()
        frm.Raise()



    def showScnBaseline(self, event):
        frm = frameWatshedBud(self, False)
        frm.Show()
        frm.Raise()



    def showScnBudget( self, event ):
        frm = frameModflowBud(self, ['Difference', 'Baseline', 'Scenario', 'Both'], False)
        frm.Show()
        frm.Raise()


    def showScnCreate( self, event ):
        frm = frameScnCreate(self)
        frm.Show()
        frm.Raise()


    def showScnGwLevel(self, event):
        frm = frameGwLevel(self, ['Difference', 'Baseline', 'Scenario'], False)
        frm.Show()
        frm.Raise()

    ###########################################################################
    ###########################################################################

    def showOnlineHelp(self, event):
        webbrowser.open(r"https://dui-gui-sustain.readthedocs.io/en/latest/")


    ###########################################################################
    ###########################################################################



def show_splash(Parent=None):
    import time
    # create, show and return the splash screen
    bitmap = wx.Bitmap('logo.png', wx.BITMAP_TYPE_PNG)
    splash = wx.adv.SplashScreen(bitmap, wx.adv.SPLASH_CENTER_ON_SCREEN | wx.adv.SPLASH_TIMEOUT, 300000, Parent)
    splash.Show()
    time.sleep(2)
    splash.Close()
    return splash

if __name__ == "__main__":
    app = wx.App()  # Create a new app, don't redirect stdout/stderr to a window.


    splash = show_splash()
    #splash.Close()

    frmMain = frameMain(None) # A Frame is a top-level window.
    frmMain.Show(True)     # Show the frame.
    # show configuration

    frm = frameConfig(frmMain, True)
    frm.ShowWindowModal()
    frm.Raise()

    app.MainLoop()
