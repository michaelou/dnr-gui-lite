
# Sustainable Use Scenario Tool for Analysis and Informing Nebraskans (*SUSTAIN*) #

![logo](logo.png)

## Introduction ##

In Nebraska, groundwater models serve as the foundation for water management plan development, implementation, and adaptation by water managers at the state and local levels by providing access to groundwater depletions, surface water use, groundwater pumping, and irrigated acreage data. Currently, there are eight groundwater models developed to cover across the state of Nebraska. A groundwater model graphic user interface (GUI) tool, Sustainable Use Scenario Tool for Analysis and Informing Nebraskans (SUSTAIN), was developed using open-source Python packages Numpy, Flopy, Pandas, Matplotlib and wxPython. SUSTAIN is aimed to disseminate hydrologic data and assist in water management planning with the natural resources districts and other stakeholders in order to balance water supplies and uses and to protect the rights of existing users of surface water and groundwater. 

## Documentation ##

https://dui-gui-sustain.readthedocs.io/en/latest/index.html
