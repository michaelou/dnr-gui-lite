# -*- coding: utf-8 -*-
"""
Created on Mon Aug 20 14:02:29 2018

@author: Michael Ou
"""

class Namefile(dict):

    def __init__(self, filePath):
        self['COMMENT'] = '# This is a NAME file created for the watershed model (RSWB).'
        with open(filePath, 'r') as f:
            lines = f.readlines()

        nComment = 0; nEmpty = 0
        for l in lines:
            l1 = l.strip()
            if l1 == '':
                self['EMPTY' + str(nEmpty)] = ''
                nEmpty += 1
                continue

            if l1[0] == '#':
                self['COMMENT' + str(nComment)] = l1
                nComment += 1
                continue


            ws = l1.split()
            self[ws[0].upper()] = l.lstrip().lstrip(ws[0]).strip()
        self['CROPS'] = self['CROP'].split()

    def writeNameFile(self, filePath):
        with open(filePath, 'w') as f:
            for i, l in self.iteritems():
                if i[:5] == 'EMPTY' or i[:7] == 'COMMENT':
                    f.write(l + '\n')
                else:
                    f.write(i + ' ' + l + '\n')

    def printname(self):
        for k, t in self.items():
            if k.startswith('EMPTY') or k.startswith('COMMENT'):
                continue
            print(k, '---', t)

    def changeMember(self):
        "this will change the CROPS of the object"
        dd = self['CROPS']
        dd.append('hello')

if __name__ == '__main__':
    n = Namefile(r'D:\UNW\gui-lite\unw-full\name-baseline.txt')
    n.printname()
    n.changeMember()
    print(n['CROPS'])
