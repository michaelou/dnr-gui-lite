# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Aug 29 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class frmMain
###########################################################################

class frmMain ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 1099,637 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		self.m_menubar1 = wx.MenuBar( 0 )
		self.m_menu5 = wx.Menu()
		self.m_menuItem1 = wx.MenuItem( self.m_menu5, wx.ID_ANY, u"Configuration", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu5.Append( self.m_menuItem1 )

		self.m_menubar1.Append( self.m_menu5, u"File" )

		self.m_menu1 = wx.Menu()
		self.m_menuItem2 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Watershed Data", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.Append( self.m_menuItem2 )

		self.m_menuItem8 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Groundwater Budget", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.Append( self.m_menuItem8 )

		self.m_menubar1.Append( self.m_menu1, u"Historical" )

		self.m_menu3 = wx.Menu()
		self.m_menuItem3 = wx.MenuItem( self.m_menu3, wx.ID_ANY, u"Baseline Data", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu3.Append( self.m_menuItem3 )

		self.m_menuItem4 = wx.MenuItem( self.m_menu3, wx.ID_ANY, u"Create Scenario", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu3.Append( self.m_menuItem4 )

		self.m_menuItem5 = wx.MenuItem( self.m_menu3, wx.ID_ANY, u"Groundwater Budget", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu3.Append( self.m_menuItem5 )

		self.m_menuItem19 = wx.MenuItem( self.m_menu3, wx.ID_ANY, u"Groundwater Levels", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu3.Append( self.m_menuItem19 )

		self.m_menubar1.Append( self.m_menu3, u"Future" )

		self.m_menu4 = wx.Menu()
		self.m_menuItem6 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"Open Online Help", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu4.Append( self.m_menuItem6 )

		self.m_menubar1.Append( self.m_menu4, u"Help" )

		self.SetMenuBar( self.m_menubar1 )


		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_MENU, self.showConfig, id = self.m_menuItem1.GetId() )
		self.Bind( wx.EVT_MENU, self.showHisWatershedBud, id = self.m_menuItem2.GetId() )
		self.Bind( wx.EVT_MENU, self.showHisModBud, id = self.m_menuItem8.GetId() )
		self.Bind( wx.EVT_MENU, self.showScnBaseline, id = self.m_menuItem3.GetId() )
		self.Bind( wx.EVT_MENU, self.showScnCreate, id = self.m_menuItem4.GetId() )
		self.Bind( wx.EVT_MENU, self.showScnBudget, id = self.m_menuItem5.GetId() )
		self.Bind( wx.EVT_MENU, self.showScnGwLevel, id = self.m_menuItem19.GetId() )
		self.Bind( wx.EVT_MENU, self.showOnlineHelp, id = self.m_menuItem6.GetId() )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def showConfig( self, event ):
		event.Skip()

	def showHisWatershedBud( self, event ):
		event.Skip()

	def showHisModBud( self, event ):
		event.Skip()

	def showScnBaseline( self, event ):
		event.Skip()

	def showScnCreate( self, event ):
		event.Skip()

	def showScnBudget( self, event ):
		event.Skip()

	def showScnGwLevel( self, event ):
		event.Skip()

	def showOnlineHelp( self, event ):
		event.Skip()


