# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Sep  7 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class frmModflowBud
###########################################################################

class frmModflowBud ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Groundwater Budget", pos = wx.DefaultPosition, size = wx.Size( 1012,638 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		self.SetBackgroundColour( wx.Colour( 171, 171, 171 ) )

		bSizer111 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer113 = wx.BoxSizer( wx.VERTICAL )

		bSizer801 = wx.BoxSizer( wx.VERTICAL )

		bSizer1132 = wx.BoxSizer( wx.VERTICAL )

		wxChkZoneChoices = [ u"County", u"NRD", u"Customized" ]
		self.wxChkZone = wx.RadioBox( self, wx.ID_ANY, u"Result Summary Zone", wx.DefaultPosition, wx.DefaultSize, wxChkZoneChoices, 1, wx.RA_SPECIFY_ROWS )
		self.wxChkZone.SetSelection( 0 )
		bSizer1132.Add( self.wxChkZone, 0, wx.ALL|wx.EXPAND, 5 )

		bSizer45 = wx.BoxSizer( wx.VERTICAL )


		bSizer1132.Add( bSizer45, 1, wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer801.Add( bSizer1132, 0, wx.EXPAND, 5 )


		bSizer113.Add( bSizer801, 1, wx.EXPAND, 5 )

		bSizer48 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button12 = wx.Button( self, wx.ID_ANY, u"Show Zones", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer48.Add( self.m_button12, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_button121 = wx.Button( self, wx.ID_ANY, u"Load Results", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer48.Add( self.m_button121, 1, wx.ALL|wx.EXPAND, 5 )


		bSizer113.Add( bSizer48, 1, wx.EXPAND, 5 )

		bSizer114 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer115 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText301 = wx.StaticText( self, wx.ID_ANY, u"Variables:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText301.Wrap( -1 )

		bSizer115.Add( self.m_staticText301, 0, wx.ALL, 5 )

		wxClbPlotVarChoices = []
		self.wxClbPlotVar = wx.CheckListBox( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wxClbPlotVarChoices, 0 )
		bSizer115.Add( self.wxClbPlotVar, 1, wx.ALL|wx.EXPAND, 5 )


		bSizer114.Add( bSizer115, 1, wx.EXPAND, 5 )

		bSizer116 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText30 = wx.StaticText( self, wx.ID_ANY, u"Zones:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText30.Wrap( -1 )

		bSizer116.Add( self.m_staticText30, 0, wx.ALL, 5 )

		wxClbPlotZoneChoices = []
		self.wxClbPlotZone = wx.CheckListBox( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wxClbPlotZoneChoices, 0 )
		bSizer116.Add( self.wxClbPlotZone, 3, wx.ALL|wx.EXPAND, 5 )


		bSizer114.Add( bSizer116, 1, wx.EXPAND, 5 )


		bSizer113.Add( bSizer114, 10, wx.EXPAND, 10 )

		bSizer117 = wx.BoxSizer( wx.VERTICAL )

		bSizer118 = wx.BoxSizer( wx.HORIZONTAL )

		wxChiPlotTypeChoices = [ u"Bar", u"Area", u"Line" ]
		self.wxChiPlotType = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wxChiPlotTypeChoices, 0 )
		self.wxChiPlotType.SetSelection( 0 )
		bSizer118.Add( self.wxChiPlotType, 1, wx.ALL, 5 )

		self.wxChkCum = wx.CheckBox( self, wx.ID_ANY, u"Cumulative", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer118.Add( self.wxChkCum, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.wxChkAggZone = wx.CheckBox( self, wx.ID_ANY, u"Sum Zones", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer118.Add( self.wxChkAggZone, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.wxChkAggVar = wx.CheckBox( self, wx.ID_ANY, u"Sum Vars", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer118.Add( self.wxChkAggVar, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )


		bSizer117.Add( bSizer118, 1, wx.EXPAND, 5 )

		bSizer119 = wx.BoxSizer( wx.HORIZONTAL )

		wxChiDfTypeChoices = [ u"Baseline", u"Scenario", u"Both", u"Difference" ]
		self.wxChiDfType = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wxChiDfTypeChoices, 0 )
		self.wxChiDfType.SetSelection( 3 )
		bSizer119.Add( self.wxChiDfType, 2, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.wxChkStacked = wx.CheckBox( self, wx.ID_ANY, u"Stacked", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer119.Add( self.wxChkStacked, 1, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.wxChkMonth = wx.CheckBox( self, wx.ID_ANY, u"Month", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer119.Add( self.wxChkMonth, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer117.Add( bSizer119, 1, wx.EXPAND, 5 )


		bSizer113.Add( bSizer117, 0, wx.EXPAND, 1 )

		bSizer120 = wx.BoxSizer( wx.VERTICAL )

		bSizer121 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText34 = wx.StaticText( self, wx.ID_ANY, u"xAxis Label", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText34.Wrap( -1 )

		bSizer121.Add( self.m_staticText34, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.wxTxtXLabel = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.wxTxtXLabel.SetMaxLength( 0 )
		bSizer121.Add( self.wxTxtXLabel, 1, wx.ALIGN_CENTER|wx.ALL, 5 )


		bSizer120.Add( bSizer121, 1, wx.EXPAND, 5 )

		bSizer122 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText341 = wx.StaticText( self, wx.ID_ANY, u"yAxis Label", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText341.Wrap( -1 )

		bSizer122.Add( self.m_staticText341, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.wxTxtYLabel = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.wxTxtYLabel.SetMaxLength( 0 )
		bSizer122.Add( self.wxTxtYLabel, 1, wx.ALIGN_CENTER|wx.ALL, 5 )


		bSizer120.Add( bSizer122, 1, wx.EXPAND, 5 )

		bSizer123 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText342 = wx.StaticText( self, wx.ID_ANY, u"Plot Title    ", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText342.Wrap( -1 )

		bSizer123.Add( self.m_staticText342, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.wxTxtPlotTitle = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.wxTxtPlotTitle.SetMaxLength( 0 )
		bSizer123.Add( self.wxTxtPlotTitle, 1, wx.ALIGN_CENTER|wx.ALL, 5 )


		bSizer120.Add( bSizer123, 1, wx.EXPAND, 5 )

		bSizer124 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button7 = wx.Button( self, wx.ID_ANY, u"Plot", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer124.Add( self.m_button7, 1, wx.ALL, 5 )

		self.m_button8 = wx.Button( self, wx.ID_ANY, u"Export Data", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer124.Add( self.m_button8, 1, wx.ALL, 5 )


		bSizer120.Add( bSizer124, 0, wx.EXPAND, 5 )


		bSizer113.Add( bSizer120, 0, wx.EXPAND, 3 )


		bSizer111.Add( bSizer113, 0, wx.EXPAND, 5 )

		bSizer125 = wx.BoxSizer( wx.VERTICAL )

		self.wxPnlPlot = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer125.Add( self.wxPnlPlot, 1, wx.EXPAND|wx.ALL, 5 )


		bSizer111.Add( bSizer125, 4, wx.EXPAND, 5 )


		self.SetSizer( bSizer111 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.wxChkZone.Bind( wx.EVT_RADIOBOX, self.changeZone )
		self.m_button12.Bind( wx.EVT_BUTTON, self.showZone )
		self.m_button121.Bind( wx.EVT_BUTTON, self.loadResults )
		self.m_button7.Bind( wx.EVT_BUTTON, self.makePlot )
		self.m_button8.Bind( wx.EVT_BUTTON, self.exportData )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def changeZone( self, event ):
		event.Skip()

	def showZone( self, event ):
		event.Skip()

	def loadResults( self, event ):
		event.Skip()

	def makePlot( self, event ):
		event.Skip()

	def exportData( self, event ):
		event.Skip()


