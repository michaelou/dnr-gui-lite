# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Sep  7 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class frmGwLevel
###########################################################################

class frmGwLevel ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Groundwater Level", pos = wx.DefaultPosition, size = wx.Size( 959,570 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.Size( 800,540 ), wx.DefaultSize )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )

		bSizer78 = wx.BoxSizer( wx.VERTICAL )

		bSizer80 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel3 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer497 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer54 = wx.BoxSizer( wx.VERTICAL )

		bSizer7111 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText343121 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"Year", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText343121.Wrap( -1 )

		self.m_staticText343121.SetMinSize( wx.Size( 60,-1 ) )

		bSizer7111.Add( self.m_staticText343121, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		wxChiPlotYearChoices = []
		self.wxChiPlotYear = wx.Choice( self.m_panel3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wxChiPlotYearChoices, 0 )
		self.wxChiPlotYear.SetSelection( 0 )
		self.wxChiPlotYear.SetMinSize( wx.Size( -1,25 ) )

		bSizer7111.Add( self.wxChiPlotYear, 1, wx.ALIGN_CENTER|wx.ALL|wx.EXPAND, 5 )


		bSizer54.Add( bSizer7111, 0, wx.EXPAND, 5 )

		bSizer71111 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText3431211 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"Month", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText3431211.Wrap( -1 )

		self.m_staticText3431211.SetMinSize( wx.Size( 60,-1 ) )

		bSizer71111.Add( self.m_staticText3431211, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		wxChiPlotMonthChoices = []
		self.wxChiPlotMonth = wx.Choice( self.m_panel3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wxChiPlotMonthChoices, 0 )
		self.wxChiPlotMonth.SetSelection( 0 )
		self.wxChiPlotMonth.SetMinSize( wx.Size( -1,25 ) )

		bSizer71111.Add( self.wxChiPlotMonth, 1, wx.ALIGN_CENTER|wx.ALL|wx.EXPAND, 5 )


		bSizer54.Add( bSizer71111, 0, wx.EXPAND, 5 )

		bSizer711 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText34312 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"Layer", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText34312.Wrap( -1 )

		self.m_staticText34312.SetMinSize( wx.Size( 60,-1 ) )

		bSizer711.Add( self.m_staticText34312, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.wxSpinLayer = wx.SpinCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 1, 10, 1 )
		bSizer711.Add( self.wxSpinLayer, 0, wx.ALL, 5 )


		bSizer54.Add( bSizer711, 0, wx.EXPAND, 5 )

		bSizer7112 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText343122 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"Color", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText343122.Wrap( -1 )

		self.m_staticText343122.SetMinSize( wx.Size( 60,-1 ) )

		bSizer7112.Add( self.m_staticText343122, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		wxChiPlotColorChoices = [ u"viridis", u"Blues", u"Greens", u"PRGn", u"PuBuGn", u"RdBu", u"RdBu_r", u"YlGnBu", u"gist_heat", u"jet_r", u"gist_rainbow", wx.EmptyString ]
		self.wxChiPlotColor = wx.Choice( self.m_panel3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wxChiPlotColorChoices, 0 )
		self.wxChiPlotColor.SetSelection( 9 )
		self.wxChiPlotColor.SetMinSize( wx.Size( -1,25 ) )

		bSizer7112.Add( self.wxChiPlotColor, 1, wx.ALIGN_CENTER|wx.ALL|wx.EXPAND, 5 )


		bSizer54.Add( bSizer7112, 0, wx.EXPAND, 5 )

		bSizer593 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText343 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"vmin", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText343.Wrap( -1 )

		self.m_staticText343.SetMinSize( wx.Size( 60,-1 ) )

		bSizer593.Add( self.m_staticText343, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.wxTxtVmin = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer593.Add( self.wxTxtVmin, 1, wx.ALIGN_CENTER|wx.ALL, 5 )


		bSizer54.Add( bSizer593, 0, wx.EXPAND, 5 )

		bSizer5931 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText34311 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"vmax", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText34311.Wrap( -1 )

		self.m_staticText34311.SetMinSize( wx.Size( 60,-1 ) )

		bSizer5931.Add( self.m_staticText34311, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.wxTxtVmax = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer5931.Add( self.wxTxtVmax, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer54.Add( bSizer5931, 0, wx.EXPAND, 5 )

		bSizer5932 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText3432 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"midpoint", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText3432.Wrap( -1 )

		self.m_staticText3432.SetMinSize( wx.Size( 60,-1 ) )

		bSizer5932.Add( self.m_staticText3432, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.wxTxtMidValue = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer5932.Add( self.wxTxtMidValue, 1, wx.ALIGN_CENTER|wx.ALL, 5 )


		bSizer54.Add( bSizer5932, 0, wx.EXPAND, 5 )

		bSizer59 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText34 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"xAxis Label", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText34.Wrap( -1 )

		self.m_staticText34.SetMinSize( wx.Size( 60,-1 ) )

		bSizer59.Add( self.m_staticText34, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.wxTxtXLabel = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer59.Add( self.wxTxtXLabel, 1, wx.ALIGN_CENTER|wx.ALL, 5 )


		bSizer54.Add( bSizer59, 0, wx.EXPAND, 5 )

		bSizer591 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText341 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"yAxis Label", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText341.Wrap( -1 )

		self.m_staticText341.SetMinSize( wx.Size( 60,-1 ) )

		bSizer591.Add( self.m_staticText341, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.wxTxtYLabel = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer591.Add( self.wxTxtYLabel, 1, wx.ALIGN_CENTER|wx.ALL, 5 )


		bSizer54.Add( bSizer591, 0, wx.EXPAND, 5 )

		bSizer592 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText342 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"Plot Title", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText342.Wrap( -1 )

		self.m_staticText342.SetMinSize( wx.Size( 60,-1 ) )

		bSizer592.Add( self.m_staticText342, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.wxTxtPlotTitle = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer592.Add( self.wxTxtPlotTitle, 1, wx.ALIGN_CENTER|wx.ALL, 5 )


		bSizer54.Add( bSizer592, 0, wx.EXPAND, 5 )

		self.m_button10 = wx.Button( self.m_panel3, wx.ID_ANY, u"Plot", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer54.Add( self.m_button10, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_button11 = wx.Button( self.m_panel3, wx.ID_ANY, u"Export as Shapefiles", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer54.Add( self.m_button11, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_button111 = wx.Button( self.m_panel3, wx.ID_ANY, u"Export as Text", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer54.Add( self.m_button111, 0, wx.ALL|wx.EXPAND, 5 )

		self.wxTxtRange = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY )
		bSizer54.Add( self.wxTxtRange, 1, wx.ALL|wx.EXPAND, 5 )


		bSizer497.Add( bSizer54, 0, wx.EXPAND, 5 )

		wxSizPlot = wx.BoxSizer( wx.VERTICAL )

		bSizer71 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText3431 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"Type", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText3431.Wrap( -1 )

		self.m_staticText3431.Hide()
		self.m_staticText3431.SetMinSize( wx.Size( 60,-1 ) )

		bSizer71.Add( self.m_staticText3431, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		wxChiPlotTypeChoices = [ u"Difference", u"Baseline", u"Scenario" ]
		self.wxChiPlotType = wx.Choice( self.m_panel3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wxChiPlotTypeChoices, 0 )
		self.wxChiPlotType.SetSelection( 0 )
		self.wxChiPlotType.Hide()
		self.wxChiPlotType.SetMinSize( wx.Size( -1,25 ) )

		bSizer71.Add( self.wxChiPlotType, 1, wx.ALIGN_CENTER|wx.ALL|wx.EXPAND, 5 )

		self.wxChkGeoRef = wx.CheckBox( self.m_panel3, wx.ID_ANY, u"GeoReference", wx.DefaultPosition, wx.Size( -1,-1 ), 0 )
		bSizer71.Add( self.wxChkGeoRef, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )


		wxSizPlot.Add( bSizer71, 0, wx.EXPAND, 5 )

		self.wxPnlPlot = wx.Panel( self.m_panel3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		wxSizPlot.Add( self.wxPnlPlot, 1, wx.EXPAND |wx.ALL, 5 )


		bSizer497.Add( wxSizPlot, 4, wx.EXPAND, 5 )


		self.m_panel3.SetSizer( bSizer497 )
		self.m_panel3.Layout()
		bSizer497.Fit( self.m_panel3 )
		bSizer80.Add( self.m_panel3, 1, wx.EXPAND |wx.ALL, 5 )


		bSizer78.Add( bSizer80, 15, wx.EXPAND, 5 )


		self.SetSizer( bSizer78 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_ACTIVATE, self.initFrm )
		self.wxChiPlotYear.Bind( wx.EVT_CHOICE, self.plotTimeChange )
		self.wxChiPlotMonth.Bind( wx.EVT_CHOICE, self.plotTimeChange )
		self.wxSpinLayer.Bind( wx.EVT_SPINCTRL, self.plotLayerChange )
		self.m_button10.Bind( wx.EVT_BUTTON, self.makePlot )
		self.m_button11.Bind( wx.EVT_BUTTON, self.exportShp )
		self.m_button111.Bind( wx.EVT_BUTTON, self.exportText )
		self.wxChiPlotType.Bind( wx.EVT_CHOICE, self.plotTypeChange )
		self.wxChkGeoRef.Bind( wx.EVT_CHECKBOX, self.showGeoRef )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def initFrm( self, event ):
		event.Skip()

	def plotTimeChange( self, event ):
		event.Skip()


	def plotLayerChange( self, event ):
		event.Skip()

	def makePlot( self, event ):
		event.Skip()

	def exportShp( self, event ):
		event.Skip()

	def exportText( self, event ):
		event.Skip()

	def plotTypeChange( self, event ):
		event.Skip()

	def showGeoRef( self, event ):
		event.Skip()


