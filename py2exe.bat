set icon=DNR.ico
set srcdir=.
set bindir=D:\UNW\gui-lite\bin
set tmpdir=D:\UNW\gui-lite\tmp

rmdir /s /q %bindir%\main
build_exe -s ^
	-c --bundle-files 3 ^
	-d %bindir%\main ^
	main.py
